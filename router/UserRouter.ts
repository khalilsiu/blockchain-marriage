import * as express from "express";
import * as path from "path";
import { Request, Response } from "express";
import { UserService } from "../service/UserService";

export class UserRouter {
  constructor(private userService: UserService) {}

  router() {
    const router = express.Router();
    router.get("/getUserID", this.getUserID);
    router.post("/charge", this.charge);
    router.get("/payment", this.payment);
    router.post('/spouse', this.createSpouse)
    router.put('/creator', this.updateCreatorInfo)
    router.put('/spouse', this.updateSpouse);
    router.get("/send_invitations", this.sendInvitations);
    router.post("/send_invitations", this.sendInvitationsPost);
    router.get("/sendUUID", this.sendUUID)
    router.post("/password/edit", this.passwordEdit)
    router.post("/search",this.search)
    router.get("/searchResult",this.searchResult)
    router.get("/search",this.searchUserName)
    router.get("/cert",this.cert)
    router.get("/getCertInfoByID",this.certInfo)
    router.get("/getCertInfoByIDPublic",this.certInfoPublic)
    router.get("/getSelfID",this.getSelfID)
    return router;
  }

  private getUserID = async (req: Request, res: Response) => {
    const { id } = req.query;
    const users = await this.userService.getUsers();
    const userdata = users.find(user => user.id == id);
    res.send(userdata);
  };

  private getSelfID = async (req: any, res: Response) => {
    let ID=req.user
    res.send(ID);
  };

  private payment = (req: Request, res: Response) => {
    res.sendFile(path.join(__dirname, "../frontend/payment/payment.html"));
  };

  private sendInvitations = (req: Request, res: Response)=>{
    res.sendFile(
      path.join(__dirname, "../frontend/payment/send_invitations.html")
    );
  }

  private charge = (req: any, res: Response) => {
    const stripe = require("stripe")(
      "sk_test_7LEUeahUQNkRaU6Dr0PZ6si500n3PPmVmF"
    );

    const token = req.body.stripeToken;
    (async () => {
      await stripe.charges.create(
        {
          amount: 9999 * 100,
          currency: "hkd",
          description: "Contract Fee",
          source: token
        },
        async (err: any, charge: any) => {
          if (charge.status == "succeeded") {
            await res.redirect("/user/send_invitations");
            let certId = (await this.userService.insertPaymentInfo(
              req.user.id,
              charge.receipt_url,
              charge.status
            ))[0];
            console.log('yocert',certId)
            let certInfo = await this.userService.getCertInfo(req.user.id);
            console.log(req.user.id)
            await this.userService.insertUUID(req.user.id, certId);
            await this.userService.sendConfirmationEmail(
              req.user.email,
              req.user.first_name,
              certInfo[0].TX,
              certInfo[0].receipt_url
            );
          }
        }
      );
    })();
  };

  private updateCreatorInfo = async (req: any, res: Response) => {
    let {
      creatorFName,
      creatorMName,
      creatorLName,
      creatorDOB,
      creatorPOB,
      creatorParent1Name,
      creatorParent2Name
    } = req.body;

    console.log('body',req.body)

    let creatorId = await this.userService.insertCreatorInfo(
      req.user.id,
      creatorFName,
      creatorMName,
      creatorLName,
      creatorDOB,
      creatorPOB,
      creatorParent1Name,
      creatorParent2Name
    );
    // console.log(creatorId);
    res.json(creatorId)
  };

  private createSpouse = async (req: Request, res: Response) => {
    let {
      spouseFName,
      spouseMName,
      spouseLName,
      spouseDOB,
      spousePOB,
      spouseParent1Name,
      spouseParent2Name
    } = req.body;

    let spouseId = await this.userService.createSpouse(
      spouseFName,
      spouseMName,
      spouseLName,
      spouseDOB,
      spousePOB,
      spouseParent1Name,
      spouseParent2Name
    );
    // console.log(spouseId);
    res.json(spouseId);
  };

  private updateSpouse = async(req:any, res: Response)=>{
    let {
      spouseFName,
      spouseMName,
      spouseLName,
      spouseDOB,
      spousePOB,
      spouseParent1Name,
      spouseParent2Name
    } = req.body;

    let spouseId = await this.userService.updateSpouse(req.user.id,
      spouseFName,
      spouseMName,
      spouseLName,
      spouseDOB,
      spousePOB,
      spouseParent1Name,
      spouseParent2Name
    );
    // console.log(spouseId);
    res.json(spouseId);
    }

  private  sendInvitationsPost = async (req: any, res: Response)=>{
    console.log(req.body.email)
    console.log(req.user.id)
    let UUID = await this.userService.getUUID(req.user.id);
    console.log(UUID)
   this.userService.sendInvitations(req.body.email,UUID[0].UUID)
   res.redirect(`/`)
  }

  private sendUUID = async (req: any, res: Response)=>{
    let data =await this.userService.sendUUID(req.user.id);
    console.log(data[0])
    res.send(data[0])
  }

  private passwordEdit = async (req: any, res: Response) => {
    await this.userService.changePassword(req.user.id,req.body.newPassword)
  }

  private search = async (req: Request, res: Response) => {
    res.redirect(`/user/searchResult/?userName=${req.body.userName}`)
  }

  private searchResult = async (req: Request, res: Response) => {
    const { userName } = req.query;
    userName
    res.sendFile(path.join(__dirname, "../frontend/user_search/search.html"))
  }

  private searchUserName = async (req: Request, res: Response) => {
    const { userName } = req.query;
    let fullName =await this.userService.searchByUserName(userName);
    if(fullName[0])
    {
    let TX=await this.userService.getTXByCreator_Id(fullName[0].id)
    if(TX[0]){res.send({data1:await this.userService.searchByID(TX[0].creator_id),data2:await this.userService.searchByID2(TX[0].spouse_id),data3:await this.userService.getUUID(fullName[0].id)})}
    let TX2=await this.userService.getTXBySpouse_Id(fullName[0].id)
    if(TX2[0]){res.send({data1:await this.userService.searchByID(TX2[0].creator_id),data2:await this.userService.searchByID2(TX2[0].spouse_id),data3:await this.userService.getUUID2(fullName[0].id)})}
    }
    else 
    {
      console.log('userName')
      let cert = await this.userService.searchByTX(userName)
      res.send({data1:await this.userService.searchByID(cert[0].creator_id),data2:await this.userService.searchByID2(cert[0].spouse_id)})
      
    }
  }

  private cert = async (req: Request, res: Response) => {
    res.sendFile(path.join(__dirname, "../frontend/Cert/cert2.html"))
  }

  private certInfo = async (req: any, res: Response) => {
    let ID =req.user.id;
    let TX=await this.userService.getTXByCreator_Id(ID)
    if(TX[0]){res.send({data1:await this.userService.searchByID(TX[0].creator_id),data2:await this.userService.searchByID2(TX[0].spouse_id)})}
    let TX2=await this.userService.getTXBySpouse_Id(ID)
    if(TX2[0]){res.send({data1:await this.userService.searchByID(TX2[0].creator_id),data2:await this.userService.searchByID2(TX2[0].spouse_id)})}
  }

  private certInfoPublic = async (req: any, res: Response) => {
    const { ID } = req.query;
    console.log(ID)
    let TX=await this.userService.getTXByCreator_Id(ID)
    if(TX[0]){res.send({data1:await this.userService.searchByID(TX[0].creator_id),data2:await this.userService.searchByID2(TX[0].spouse_id)})}
    let TX2=await this.userService.getTXBySpouse_Id(ID)
    if(TX2[0]){res.send({data1:await this.userService.searchByID(TX2[0].creator_id),data2:await this.userService.searchByID2(TX2[0].spouse_id)})}
  }

}
