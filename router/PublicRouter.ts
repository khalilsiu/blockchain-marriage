import * as express from 'express';
import * as path from "path";
import { Request, Response } from 'express';
import { PublicService } from '../service/PublicService';


export class PublicRouter {
    
        constructor(private publicService:PublicService)
        {
     
        }

    router() {
        const router = express.Router();
        router.get('/cert', this.cert);
        router.get('/getCertInfoByIDPublic',this.certInfoPublic)
        return router;
    }
    
    private cert = (req: Request, res: Response) => {
        res.sendFile(path.join(__dirname, "../public/cert/cert.html"))
    }


  private certInfoPublic = async (req: any, res: Response) => {
    const { ID } = req.query;
    console.log('get')
    let Creator_Id= await this.publicService.getCommited_cert_numberByCreator_Id(ID)
        let Spouse_Id= await this.publicService.getCommited_cert_numberBySpouse_Id(ID)
         if(Creator_Id[0])
         {
            console.log(Creator_Id[0].commited_cert_number)
            res.send(await this.publicService.getInfoFromBlockChain(Creator_Id[0].commited_cert_number))
         }
         else if(Spouse_Id[0]){
            console.log(Spouse_Id[0].commited_cert_number)
             res.send(await this.publicService.getInfoFromBlockChain(Spouse_Id[0].commited_cert_number))
         }
    
  }

}

