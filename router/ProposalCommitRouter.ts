import { ProposalCommitService } from "../service/ProposalCommitService";
import * as express from 'express'
import { ProposalLinkService } from "../service/ProposalLinkService";


interface Certificate {
    marriage_location_longitude: number;
    marriage_location_latitude: number;
    marriage_date: number;
    witness_name: string;
    creator_vow: string;
    spouse_vow: string;
    e_signature_1: string;
    e_signature_2: string;
    e_signature_witness: string;
    c_full_name: string;
    c_date_of_birth: number;
    c_place_of_birth: string;
    c_parent1_name: string;
    c_parent2_name: string;
    s_full_name:string;
    s_date_of_birth: number;
    s_place_of_birth: string;
    s_parent1_name: string;
    s_parent2_name: string;
    creation_date: number;
    certificate_id: number;

}
export class ProposalCommitRouter{
    constructor(private proposalCommitService: ProposalCommitService, private proposalLinkService: ProposalLinkService){
        console.log(this.proposalLinkService)
        console.log(this.proposalCommitService)
    }

    public router(){
        const router = express.Router();
        router.get('/committed', this.checkCommitted);
        router.post('/commit', this.commit);
        return router;
    }

    private commit = async(req: express.Request, res: express.Response)=>{
        let data = await this.proposalLinkService.getCert(req.body.UUID);
        console.log(data);
      let certificate: Certificate = {
        marriage_location_longitude: 0,
        marriage_location_latitude: 0,
        marriage_date: 0,
        witness_name: '',
        creator_vow: '',
        spouse_vow: '',
        e_signature_1: '',
        e_signature_2: '',
        e_signature_witness: '',
        c_full_name: '',
        c_date_of_birth: 0,
        c_place_of_birth: '',
        c_parent1_name: '',
        c_parent2_name: '',
        s_full_name: '',
        s_date_of_birth: 0,
        s_place_of_birth: '',
        s_parent1_name: '',
        s_parent2_name: '',
        creation_date: 0,
        certificate_id: 0
      };

      if(data){
          for (let entry of data) {
            certificate["marriage_location_longitude"] =
            parseInt((parseFloat(entry.marriage_location_longitude) * 10000).toFixed(0))
            certificate["marriage_location_latitude"] =
           parseInt((parseFloat(entry.marriage_location_latitude) * 10000).toFixed(0));
            certificate["marriage_date"] = new Date(entry.marriage_date).getTime();
            certificate["witness_name"] = entry.witness_name;
            certificate["creator_vow"] = entry.creator_vow;
            certificate["spouse_vow"] = entry.spouse_vow;
            certificate["e_signature_1"] = entry.e_signature_1;
            certificate["e_signature_2"] = entry.e_signature_2;
            certificate["e_signature_witness"] = entry.e_signature_witness;
            certificate["c_full_name"] = entry.c_first_name + " " + entry.c_middle_name + " " + entry.c_last_name
            certificate["c_date_of_birth"] = new Date(entry.c_date_of_birth).getTime();
            certificate["c_place_of_birth"] = entry.c_place_of_birth;
            certificate["c_parent1_name"] = entry.c_parent1_name;
            certificate["c_parent2_name"] = entry.c_parent2_name;

            certificate["s_full_name"] = entry.s_first_name+ " " + entry.s_middle_name + " " + entry.s_last_name
            certificate["s_date_of_birth"] = new Date(entry.s_date_of_birth).getTime();
            certificate["s_place_of_birth"] = entry.s_place_of_birth;
            certificate["s_parent1_name"] = entry.s_parent1_name;
            certificate["s_parent2_name"] = entry.s_parent2_name;
            certificate['certificate_id'] = entry.certificate_id;
          }
          certificate['creation_date'] = Date.now();
      }
      console.log('commit',certificate)
        let id = await this.proposalCommitService.commit(certificate);
        res.json(id);

    }

    private checkCommitted = (req: express.Request, res: express.Response) =>{
      let committed = this.proposalCommitService.checkCommitted();
      console.log('router committed', committed)
      res.json(committed);
    }
}