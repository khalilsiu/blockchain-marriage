import * as express from "express";
import multer = require("multer");
import { StoryService } from "../service/StoryService";

export class StoryRouter {

  constructor(
    private upload: multer.Instance,
    private storyService: StoryService
  ) {

  }
  public router() {
    const router = express.Router();
    router.post("/story", this.storyUpload);
    router.put('/story', this.updateStory);
    router.post('/photo', this.upload.array('photo'), this.photoUpload)
    router.put('/photo', this.upload.array('photo'), this.updatePhoto)
    return router;
  }

  private storyUpload = async (req: express.Request, res: express.Response) => {
    let storyIds = []
    let {
      certId,
      description,
      location,
      placeId,
      locationLat,
      locationLng
    } = req.body;
    let storyId = await this.storyService.storySave(
      certId,
      description,
      location,
      placeId,
      locationLat,
      locationLng
    );
    storyIds.push(storyId)
    res.json(storyIds);
  };

  private updateStory = async(req: express.Request, res: express.Response) =>{
    let ids = [];
    let {
      storyIds,
      certId,
      description,
      location,
      placeId,
      locationLat,
      locationLng
    } = req.body;
    // console.log("storyIdssss",storyIds)
    for (let i = 0; i < storyIds.length; i++){
      let id = await this.storyService.updateStory(
        storyIds[i],
        certId,
        description,
        location,
        placeId,
        locationLat,
        locationLng
      );
      // console.log('IIIIIII', id)
      ids.push(id)
    }
    res.json(ids);
  }

  private photoUpload = async (req: express.Request, res: express.Response) =>{

      let {storyIds, order} = req.body
      // console.log('photoupload',storyIds)
      storyIds = JSON.parse(storyIds);
      // console.log('dllm',storyIds, order);
      order = JSON.parse(order);
      for (let index in req.files as any as Express.Multer.File[]){
        let indexOf = req.files[index].path.indexOf('uploads')
        let path = req.files[index].path.slice(indexOf)
        await this.storyService.photoSave(storyIds, path, order[index])
      }
      res.json({result:'success'})
  }

  private updatePhoto = async (req: express.Request, res: express.Response) =>{
      let {ids, originalIds, order, storyIds} = req.body;
      ids = JSON.parse(ids)
      originalIds = JSON.parse(originalIds);
      order = JSON.parse(order);
      storyIds = JSON.parse(storyIds);
      for (let index = 0; index<originalIds.length; index ++){
          if(ids.includes(originalIds[index])){
            // console.log('originallsss',originalIds[index], ids.indexOf(originalIds[index]))
              let id = await this.storyService.updatePhoto(originalIds[index], ids.indexOf(originalIds[index]))
              console.log(id)
          }
          else{
              let id = await this.storyService.removePhoto(originalIds[index]);
              console.log(id);
          }
      }
      for(let index in req.files as any as Express.Multer.File[]){
          let indexOf = req.files[index].path.indexOf('uploads');
          let path = req.files[index].path.slice(indexOf);
          // console.log('fuckkkk',storyIds[0], path, order[index])
          await this.storyService.photoSave(storyIds[0], path, order[index]);
      }
      res.json({result:'success'})

  }
}
