import * as express from 'express';
import { Request, Response } from 'express';
import { LoginService } from '../service/LoginService';


export class LoginRouter {
    

        constructor(private loginService:LoginService)
        {
     
        }

    router() {
        const router = express.Router();
        router.post('/reg', this.reg);
        router.get('/getUserID', this.getUserID);
        return router;
    }
    
    private reg = (req: Request, res: Response) => {
        const userInfo= req.body
        console.log(userInfo)
        this.loginService.createUser(userInfo.userName,userInfo.password,userInfo.first_name,userInfo.middleName,userInfo.lastName,userInfo.email)
        res.redirect('/')
    }

    private  getUserID = async(req: Request, res: Response) => {
        const { id } = req.query;
        const users = await this.loginService.getUsers();
        const userdata = users.find(user => user.id == id);
        res.send(userdata)}

}

