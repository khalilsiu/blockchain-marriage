import * as express from 'express';
import { Request, Response } from 'express';
import { GetInfoFromBlockChainService } from '../service/GetInfoFromBlockChainService';


export class GetInfoFromBlockChainRouter {
    

        constructor(private getInfoFromBlockChainService:GetInfoFromBlockChainService)
        {
     
        }

    router() {
        const router = express.Router();
        router.get('/getCertInfo', this.getCertInfo);
        return router;
    }
    
    private  getCertInfo = async(req:Request, res: Response) => {
        const ID=req.user.id;
        let Creator_Id= await this.getInfoFromBlockChainService.getCommited_cert_numberByCreator_Id(ID)
        let Spouse_Id= await this.getInfoFromBlockChainService.getCommited_cert_numberBySpouse_Id(ID)
         if(Creator_Id[0])
         {
            console.log(Creator_Id[0].commited_cert_number)
            res.send(await this.getInfoFromBlockChainService.getInfoFromBlockChain(Creator_Id[0].commited_cert_number))
         }
         else if(Spouse_Id[0]){
            console.log(Spouse_Id[0].commited_cert_number)
             res.send(await this.getInfoFromBlockChainService.getInfoFromBlockChain(Spouse_Id[0].commited_cert_number))
         }
    }


}