import * as express from 'express';
import { Request, Response } from 'express';
import { GenPDFService } from '../service/GenPDFService';


export class GenPDFRouter {
    

        constructor(private genPDFService:GenPDFService)
        {
     
        }

    router() {
        const router = express.Router();
        router.get('/genPDF', this.genPDF);
        router.get('/download/PDF', this.download);
        return router;
    }
    
    private  genPDF = async(req:any, res: Response) => {
        await this.genPDFService.genPDF(req.user.id,)
    }

    private download =async (req: Request, res: Response) => {
        await this.genPDFService.genPDF(req.user.id,)
        await res.download(req.user.PDF_path)
    }

}

