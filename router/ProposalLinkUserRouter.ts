import * as express from "express";
import {Response} from "express";
import { UserService } from "../service/UserService";

export class ProposalLinkUserRouter {
  constructor(private userService: UserService) {}

  router() {
    const router = express.Router();
    router.put('/creator', this.updateCreatorInfo)
    router.put('/spouse', this.updateSpouse);
    return router;
  }

  private updateCreatorInfo = async (req: any, res: Response) => {
    let {
        creatorId,
      creatorFName,
      creatorMName,
      creatorLName,
      creatorDOB,
      creatorPOB,
      creatorParent1Name,
      creatorParent2Name
    } = req.body;

    console.log('body',req.body)

    let entry = await this.userService.insertCreatorInfo(
      creatorId,
      creatorFName,
      creatorMName,
      creatorLName,
      creatorDOB,
      creatorPOB,
      creatorParent1Name,
      creatorParent2Name
    );
    // console.log(creatorId);
    res.json(entry)
  };


  private updateSpouse = async(req:any, res: Response)=>{
    let {
        creatorId,
      spouseFName,
      spouseMName,
      spouseLName,
      spouseDOB,
      spousePOB,
      spouseParent1Name,
      spouseParent2Name
    } = req.body;

    let entry = await this.userService.updateSpouse(creatorId,
      spouseFName,
      spouseMName,
      spouseLName,
      spouseDOB,
      spousePOB,
      spouseParent1Name,
      spouseParent2Name
    );
    // console.log(spouseId);
    res.json(entry);
    }


}
