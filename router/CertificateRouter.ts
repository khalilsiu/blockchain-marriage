import * as express from "express";
import { CertificateService } from "../service/CertificateService";
import fetch from "cross-fetch";

export class CertificateRouter {

  constructor(
    private certificateService: CertificateService
  ) {
  }

  public router() {
    const router = express.Router();
    // form data has to be read using multer, bodyparser urlencoded cannot be read
    router.post("/certificate", this.createCert);
    router.put('/certificate', this.updateCert)
    router.get("/location", this.getLocation);
    router.get("/certificate", this.getCert);
    router.get('/paid-status', this.checkPaid)
    return router;

  }

  private createCert = async (req: express.Request, res: express.Response) => {

    let {
      spouseId,
      marriageLocationLat,
      marriageLocationLng,
      marriageDate,
      marriageLocation,
      witnessName,
      creatorVow,
      spouseVow,
      creatorSign,
      spouseSign,
      witnessSign
    } = req.body;

    let certId = await this.certificateService.createCert(
      req.user.id,
      spouseId,
      marriageLocationLat,
      marriageLocationLng,
      marriageDate,
      marriageLocation,
      witnessName,
      creatorVow,
      spouseVow,
      creatorSign,
      spouseSign,
      witnessSign
    );

    res.json(certId);
  };

  private updateCert = async(req: express.Request, res: express.Response) =>{
    let {
      certId,
      marriageLocationLat,
      marriageLocationLng,
      marriageDate,
      marriageLocation,
      witnessName,
      creatorVow,
      spouseVow,
      creatorSign,
      spouseSign,
      witnessSign
    } = req.body;

    let returnId = await this.certificateService.updateCert(
      certId,
      marriageLocationLat,
      marriageLocationLng,
      marriageDate,
      marriageLocation,
      witnessName,
      creatorVow,
      spouseVow,
      creatorSign,
      spouseSign,
      witnessSign
    );

    res.json(returnId);
  }
  private getCert = async (req: express.Request, res: express.Response) => {

    let data = await this.certificateService.getCert(req.user.id);
    let certificate = {};
    let photos = [];
    let photoIds = []
    let photoIdMap = {};
    let storyIds = []
    for (let entry of data){
        if (entry.photo){
          let index = entry.photo.indexOf('uploads');
          let path = entry.photo.slice(index)
          photos.push(path);
          photoIdMap[entry.photo] = entry.photos_id;
          photoIds.push(entry.photos_id);
        }
        storyIds.push(entry.stories_id);
        certificate['marriage_location_longitude'] =  entry.marriage_location_longitude;
        certificate['marriage_location_latitude'] =  entry.marriage_location_latitude;
        certificate['marriage_date'] =  entry.marriage_date;
        certificate['witness_name'] =  entry.witness_name;
        certificate['creator_vow'] =  entry.creator_vow;
        certificate['spouse_vow'] =  entry.spouse_vow;
        certificate['e_signature_1'] =  entry.e_signature_1;
        certificate['e_signature_2'] =  entry.e_signature_2;
        certificate['e_signature_witness'] =  entry.e_signature_witness;
        certificate['location'] =  entry.location;
        certificate['c_first_name'] =  entry.c_first_name;
        certificate['c_middle_name'] =  entry.c_middle_name;
        certificate['c_last_name'] =  entry.c_last_name;
        certificate['c_date_of_birth'] =  entry.c_date_of_birth;
        certificate['c_place_of_birth'] =  entry.c_place_of_birth;
        certificate['c_parent1_name'] =  entry.c_parent1_name
        certificate['c_parent2_name'] =  entry.c_parent2_name;
        certificate['c_email'] =  entry.c_email;
        certificate['s_first_name'] =  entry.s_first_name;
        certificate['s_middle_name'] =  entry.s_middle_name;
        certificate['s_last_name'] =  entry.s_last_name;
        certificate['s_date_of_birth'] =  entry.s_date_of_birth;
        certificate['s_place_of_birth'] =  entry.s_place_of_birth;
        certificate['s_parent1_name'] =  entry.s_parent1_name;
        certificate['s_parent2_name'] =  entry.s_parent2_name;
        certificate['description'] =  entry.description;
        certificate['longitude'] =  entry.longitude;
        certificate['latitude'] =  entry.latitude;
        certificate['place_id'] =  entry.place_id;
        certificate['stories_id'] =  entry.stories_id;
        certificate['certificate_id'] = entry.certificate_id
        certificate['description'] = entry.description;
    }
    certificate['photo'] = photos;
    certificate['photoIdMap'] = photoIdMap;
    certificate['photoIds'] = photoIds;
    certificate['storyIds'] = Array.from(new Set(storyIds));
    res.json(certificate);
  };

  private getLocation = async (req: express.Request, res: express.Response) => {

    let key = "AIzaSyAdi1X1UKxYMBB_2w0hbM3qsl9UEo40oSo";
    let placeId;
    req.query.place_id
      ? (placeId = req.query.place_id)
      : (placeId = "ChIJtTeDfh9w5kcRJEWRKN1Yy6I");


    let url = `https://maps.googleapis.com/maps/api/place/details/json?placeid=${placeId}&fields=name,address_component,rating,formatted_phone_number,geometry&key=${key}`;
    let locationRes = await fetch(url, {
      method: "get"
    });
    let locationAddressRes = (await locationRes.json()).result;

    res.json(locationAddressRes);
  };

  private checkPaid = async(req: express.Request, res: express.Response) =>{
    console.log("checkpaid",req.query)
    let paidStatus = await this.certificateService.checkPaid(req.query.certId);
    res.json(paidStatus);
  }
}
