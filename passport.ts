import * as passport from "passport";
import * as passportLocal from "passport-local";
import * as passportOauth2 from "passport-oauth2";
import { checkPassword} from "./hash";
import fetch from "cross-fetch";
import { LoginService } from "./service/LoginService";
import * as Knex from 'knex'
const knexConfig = require('./knexfile');
//const FacebookTokenStrategy = require('passport-facebook-token');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])

let loginService = new LoginService(knex);


const LocalStrategy = passportLocal.Strategy;
const OAuth2Strategy = passportOauth2.Strategy;

passport.use(
  new LocalStrategy(async function(username, password, done) {
    console.log('Google')
    const users = await loginService.getUsers();
    const user = users.find(user => user.username == username);
    
    if (!user) {
      return done(new Error("User or Password Incorrect"));
    }
    const match = await checkPassword(password, user.password);
    if (match) {
      return done(null, user);
    } else {
      return done(new Error("User or Password Incorrect"));
    }
  })
);


passport.use('google',new OAuth2Strategy({
    authorizationURL: 'https://accounts.google.com/o/oauth2/auth',
    tokenURL:"https://accounts.google.com/o/oauth2/token",
    clientID:"299134924043-6brsr0ab3kjh4t0g7451btul57ikkitc.apps.googleusercontent.com",
    clientSecret: "22Jjzvm9zvVCchC_A3dwiAo7",
    callbackURL: "/auth/google/callback"
  },
  async function(accessToken:string, refreshToken:string, profile:any, done:Function) {
    const res = await fetch('https://www.googleapis.com/oauth2/v2/userinfo',{
        method:"get",
        headers:{
            "Authorization":`Bearer ${accessToken}`
        }
    });

    const result = await res.json();
    const users = await loginService.getUsers();
    console.log(result)
    let user = users.find((user)=>user.email == result.email);
    console.log(user)
    console.log(result.email)
    if(!user){  
      done(new Error("User not Found"));
    }
    done(null,{accessToken,refreshToken,id:user.id})
  }
));


passport.serializeUser(function(user: { id: number }, done) {
  done(null, user.id);
});

passport.deserializeUser(async function(id, done) {
  const users = await loginService.getUsers();
  const user = users.find(user => id == user.id);
  if (user) {
    done(null, user);
  } else {
    done(new Error("User not Found"));
  }
});
