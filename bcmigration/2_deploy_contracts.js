 // number the file so that truffle knows the order

// artifacts: contract abstraction specific to truffle, so that we can interact with the smart contract
var Certificates = artifacts.require("./Certificates.sol");

module.exports = function(deployer) {
  deployer.deploy(Certificates);
};

// in console run truffle console, to interact with the contract
// run Election.deployed().then(function(instance){app=instance})
// Election is the one that is defined here
// deployed() gets a copy of the deployed abstraction
// the process is async >> needs to be a promise, then use .then()
