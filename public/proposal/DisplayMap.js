class DisplayMap {
  constructor() {
    this.labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    this.labelIndex = 0;
    this.map;
    this.eiffel = { lat: 48.8584, lng: 2.2945 };
    this.initMap(this.eiffel);
    this.lat;
    this.lng;
  }

  initMap = async (coordinates, placeId) => {
    var input = document.querySelector("#marriage-location");
    // instantiate an autocomplete object that takes the input from html
    var autocomplete = new google.maps.places.Autocomplete(input);
    var place = autocomplete.getPlace();

    if (!placeId) {
      let locationRes = await fetch("/proposal-cert/location");
      this.initialPlace = await locationRes.json();
    } else {
      let locationRes = await fetch(`/proposal-cert/location?place_id=${placeId}`);
      this.initialPlace = await locationRes.json();
    }
    // infowindow object to popup when selected
    var infowindow = new google.maps.InfoWindow();
    // display back the content to html using setContent
    let infowindowContent = document.querySelector("#infowindow-content");
    infowindow.setContent(infowindowContent);

    this.map = new google.maps.Map(document.querySelector("#map"), {
      center: coordinates,
      zoom: 15
    });
    var marker = new google.maps.Marker({
      map: this.map,
      anchorPoint: new google.maps.Point(0, -29),
      position: coordinates
    });

    let address = "";
    address = [
      (this.initialPlace.address_components[0] &&
        this.initialPlace.address_components[0].short_name) ||
        "",
      (this.initialPlace.address_components[1] &&
        this.initialPlace.address_components[1].short_name) ||
        "",
      (this.initialPlace.address_components[2] &&
        this.initialPlace.address_components[2].short_name) ||
        ""
    ].join(" ");

    let url =
      "https://maps.gstatic.com/mapfiles/place_api/icons/jewelry-71.png";

    infowindowContent.children["place-icon"].src = url;
    infowindowContent.children[
      "place-name"
    ].textContent = this.initialPlace.name;
    infowindowContent.children["place-address"].textContent = address;
    
    infowindow.open(map, marker);
    this.lat = this.initialPlace.geometry.location.lat;
    this.lng = this.initialPlace.geometry.location.lng;
    console.log(this.lat, this.lng)

    autocomplete.addListener("place_changed", () => {
      infowindow.close();
      marker.setVisible(false);

      // getPlace() gets the place from the autocomplete object
      var place = autocomplete.getPlace();

      this.lat = place.geometry.location.lat();
      this.lng = place.geometry.location.lng();
      this.placeId = place.place_id;
      // console.log(place.geometry.location.lat(),place.geometry.location.lng())

      // if the geometry exists, will fit the bound at the viewport
      if (place.geometry.viewport) {
        this.map.fitBounds(place.geometry.viewport);
      } else {
        this.map.setCenter(place.geometry.location);
        this.map.setZoom(16); // Why 17? Because it looks good.
      }
      marker.setPosition(place.geometry.location);

      marker.setVisible(true);

      var address = "";
      // Joins the place.address_components together to get the address
      if (place.address_components) {
        address = [
          (place.address_components[0] &&
            place.address_components[0].short_name) ||
            "",
          (place.address_components[1] &&
            place.address_components[1].short_name) ||
            "",
          (place.address_components[2] &&
            place.address_components[2].short_name) ||
            ""
        ].join(" ");
      }
      let url =
        "https://maps.gstatic.com/mapfiles/place_api/icons/jewelry-71.png";

      infowindowContent.children["place-icon"].src = url;
      infowindowContent.children["place-name"].textContent = place.name;
      infowindowContent.children["place-address"].textContent = address;

      infowindow.open(map, marker);
    });
  };
}
