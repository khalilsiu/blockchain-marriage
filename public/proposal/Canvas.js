

class Canvas{

    constructor(){
        this.canvases = document.querySelectorAll("canvas");
        this.crosses = document.querySelectorAll('.sign-clear')
        this.signaturePad = []
        for(let i = 0; i < this.canvases.length; i++){
            this.signaturePad[i] = new SignaturePad(this.canvases[i],{
                minWidth: 1,
                maxWidth: 1,
                penColor: "black"
            });
            this.canvases[i].height = this.canvases[i].offsetHeight;
            this.canvases[i].width = this.canvases[i].offsetWidth;
        }

        this.signaturePad[0].off();
        for (let cross of this.crosses){
            cross.addEventListener('click',(event)=> this.clearSignature(event))
        }
        
    }

    clearSignature = (event)=>{
        
        console.log(event.currentTarget.dataset.id)
        this.signaturePad[event.currentTarget.dataset.id].clear();
    }





}