

class Canvas{

    constructor(){
        this.canvases = document.querySelectorAll("canvas");
        this.crosses = document.querySelectorAll('.sign-clear')
        this.check = document.querySelector('.sign-check')
        this.signaturePad = []
        for(let i = 0; i < this.canvases.length; i++){
            this.signaturePad[i] = new SignaturePad(this.canvases[i],{
                minWidth: 1,
                maxWidth: 1,
                penColor: "black"
            });
            this.canvases[i].height = this.canvases[i].offsetHeight;
            this.canvases[i].width = this.canvases[i].offsetWidth;
            this.signaturePad[i].off()
        }



        
    }

}