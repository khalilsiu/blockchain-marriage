class DragAndDrop {
  constructor() {
    this.dropZone = document.querySelector("#drop-zone");
    this.dropZone.addEventListener("drop", event => this.dropHandler(event));
    this.dropZone.addEventListener("dragover", event =>
      this.dragOverHandler(event)
    );
    this.carouselArea = document.querySelector("#carousel-area");
    this.filesUrls = [];
  }

  dropHandler = event => {
    console.log("File(s) dropped");
    let imageTypes = [
      "image/png",
      "image/gif",
      "image/bmp",
      "image/jpg",
      "image/jpeg"
    ];
    // Prevent default behavior (Prevent file from being opened)
    event.preventDefault();
    if (event.dataTransfer.files) {
      // Use DataTransferItemList interface to access the file(s)
      for (var i = 0; i < event.dataTransfer.files.length; i++) {
        // If dropped items aren't files, reject them
        if (!imageTypes.includes(event.dataTransfer.files[i].type)) {
          alert("Only accept png, gif, bmp, jpg and jpeg files.");
        } else {
          this.filesUrls[this.filesUrls.length] = event.dataTransfer.files[i];
        }
      }
      console.log(this.filesUrls);
      this.populateCarousel(this.filesUrls, window.loadCertificate.photoIdMap);
    }
  };
  dragOverHandler(event) {
    // Prevent default behavior (Prevent file from being opened)
    event.preventDefault();
  }

  populateCarousel = (filesUrls, photoIdMap) => {
    let storyIds = []
    if (window.loadCertificate.storyIds){
      storyIds = window.loadCertificate.storyIds
    }
    else{
      storyIds[0] = 1
    }
    let firstObjectURL;
    console.log(filesUrls[0])
    console.log(photoIdMap)
    let photoId;
    if (typeof(filesUrls[0]) == 'string'){
        firstObjectURL = filesUrls[0]
        photoId = photoIdMap[firstObjectURL]
    }
    else{
      console.log(typeof(firstObjectURL));

      firstObjectURL = URL.createObjectURL(filesUrls[0])
    }
    let inner = `
    <div id="drop-zone" data-story-id='${storyIds[0]}'>
    <div id="index-carousel" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
          <li data-target="#index-carousel" data-slide-to="0" class="active"></li>`;

    if (filesUrls && filesUrls.length > 1) {
      for (let i = 1; i < filesUrls.length; i++) {
        inner += `<li data-target="#index-carousel" data-slide-to="${i}"></li>`;
      }
    }
    inner += `
      </ol>
      <div class="carousel-inner">
      <div class="carousel-item active">
        <div class='background non-modal' data-target="#modal" data-toggle="modal" data-photo-id='${photoId}' data-id='0'style="background-image: url(../${firstObjectURL})"></div>
      </div>`;

    
    if (filesUrls && filesUrls.length > 1) {
      for (let i = 1; i < filesUrls.length; i++) {
        let objectURL;
        let photoId;
        if (typeof(filesUrls[i]) == 'string'){
          objectURL = filesUrls[i]
          photoId = photoIdMap[firstObjectURL]
        }
        else{
          objectURL = URL.createObjectURL(filesUrls[i])
        }
        inner += `
        <div class="carousel-item">
          <div class='background' data-target="#modal" data-toggle="modal" data-photo-id='${photoId}' data-id='${i}' style="background-image: url(../${objectURL})"></div>
        </div>`;
      }
    }
    inner += `
      </div>
        <a class="carousel-control-prev" href="#index-carousel" role="button" data-slide="prev">
          <div class='arrow-container'>    
            <span><i class="fa fa-angle-left arrows" aria-hidden="true"></i></span>
          </div>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#index-carousel" role="button" data-slide="next">
          <div class='arrow-container'>    
            <span><i class="fa fa-angle-right arrows" aria-hidden="true"></i></span>
          </div>
            <span class="sr-only">Next</span>
        </a>
      </div>
      </div>`;

    inner += `<div id='modal-container'></div>`;
    this.carouselArea.innerHTML = inner;

    document.querySelector("#drop-zone").addEventListener("click", event => {
      if (event.target && event.target.matches(".background")) {
        this.createModal(filesUrls, photoIdMap);
      }
    });
    this.dropZone = document.querySelector("#drop-zone");
    this.dropZone.addEventListener("drop", event => this.dropHandler(event));
    this.dropZone.addEventListener("dragover", event =>
      this.dragOverHandler(event)
    );
  };

  createModal = (filesUrls, photoIdMap) => {
    let firstObjectURL;
    let photoId;
    if (typeof(filesUrls[0])=='string'){
      firstObjectURL = filesUrls[0]
      photoId = photoIdMap[firstObjectURL]
    }
    else{
      firstObjectURL = URL.createObjectURL(filesUrls[0])
    }
    // let url = target.style.backgroundImage.slice(4, -1).replace(/"/g, "");
    let modalContainer = document.querySelector("#modal-container");
    let inner = `
    <div aria-hidden="true" aria-labelledby="myModalLabel" class="modal fade" id="modal" role="dialog" tabindex="-1">
      <div class="modal-dialog modal-xl bigger center" role="document">
        <div class="modal-content" id="index-modal">
          <div class="modal-body mb-0 p-0">`;
    inner += `
              <span><i id="close-btn" class="fa fa-times" aria-hidden="true" data-dismiss="modal"></i></span>
            <div id="modal-carousel" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#modal-carousel" data-slide-to="0" class="active"></li>`;

    if (filesUrls && filesUrls.length > 1) {
      for (let i = 1; i < filesUrls.length; i++) {
        inner += `
                <li data-target="#modal-carousel" data-slide-to="${i}"></li>`;
      }
    }
    inner += `
              </ol>
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <div class='background modal-pics' data-photo-id='${photoId}' data-id='0'style="background-image: url(../${firstObjectURL})"></div>
                </div>`;

if (filesUrls && filesUrls.length > 1) {
      for (let i = 1; i < filesUrls.length; i++) {
        let objectURL;
        let photoId;
        if (typeof(filesUrls[i])=='string'){
          objectURL = filesUrls[i]
          photoId = filesUrls[objectURL]
        }
        else{
          objectURL = URL.createObjectURL(filesUrls[i])
        }
        inner += `
                <div class="carousel-item">
                  <div class='background modal-pics' data-photo-id='${photoId}' data-id='${i}' style="background-image: url(../${objectURL})"></div>
                </div>`;
      }
    }
    inner += `
              </div>
                <a class="carousel-control-prev" href="#modal-carousel" role="button" data-slide="prev">
                    <div class='arrow-container'>    
                      <span><i class="fa fa-angle-left arrows" aria-hidden="true"></i></span>
                    </div>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#modal-carousel" role="button" data-slide="next">
                    <div class='arrow-container'>    
                      <span><i class="fa fa-angle-right arrows" aria-hidden="true"></i></span>
                    </div>
                    <span class="sr-only">Next</span>
                </a>
              </div>
            </div>`;

    inner += `
          </div>
        </div>
      </div>
    </div>`;

    modalContainer.innerHTML = inner;
  };
}
