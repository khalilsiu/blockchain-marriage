
class CommitToChain{
    constructor(){

    }

    commit = async () =>{
        this.confirmationBox = document.querySelector('#confirmation-box');
        let inner = `
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="confirmation-modal-label">Committing to Ethereum</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="confirmation-modal">
                    <div id='spinner-container'>
                        <img src="Spinner-1s-200px.gif" width="250px">
                    </div>  
                </div>
            </div>`
        this.confirmationBox.innerHTML = inner;
        
        let id = await fetch('/proposal-commit/commit',{
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
              },
            body: JSON.stringify({
                UUID:window.loadCertificate.UUID
            })
        })
    
        $('#confirmation-modal-container').data('bs.modal')._config ={ keyboard: false, backdrop: 'static'}
        console.log(id);
        this.checkCommitted()
    }

    checkCommitted = async () =>{
        setTimeout(async ()=>{
            console.log('check')
            let committedRes = await fetch('/proposal-commit/committed');
            this.committed = await committedRes.json()
           console.log(this.committed)
            if (!this.committed){
                this.checkCommitted();
            }
            else{
                window.location.href = `committed/?ID=${this.UUID}`
            }
        },2000)
        
    
    }
}


// This does not work as the callback would run last before all the non-callback codes
//      it would infinitely loop itself before the callback is being run
// checkCommitted = async () =>{
//     setTimeout(()=>{
//         this.committed = await fetch('/proposal-commit/committed');
//     },2000)
//     if (!this.committed){
//        this.checkCommitted();
//      }

// This wont run too..
// while(!this.committed){
//     setTimeout(()=>{
//         this.committed = await fetch('/proposal-commit/committed')
//     },2000)
// }