

class LoadCertificate{
    constructor(){
        let indexOf = window.location.href.indexOf('committed')
        let subUrl = window.location.href.slice(indexOf)
        let searchParams = new URLSearchParams(subUrl);
        this.UUID = searchParams.get('committed/?ID');
        console.log(this.UUID)
        
        this.load();
    }
    load = async ()=>{
        this.creatorFName = document.querySelector('#creator-fname');
        this.creatorMName = document.querySelector('#creator-mname');
        this.creatorLName = document.querySelector('#creator-lname');
        this.creatorDOB = document.querySelector('#creator-dob');
        this.creatorPOB = document.querySelector('#creator-pob');
        this.creatorParent1Name = document.querySelector('#creator-parent1-name');
        this.creatorParent2Name = document.querySelector('#creator-parent2-name')
        this.spouseFName = document.querySelector('#spouse-fname')
        this.spouseMName = document.querySelector('#spouse-mname');
        this.spouseLName = document.querySelector('#spouse-lname');
        this.spouseDOB = document.querySelector('#spouse-dob');
        this.spousePOB = document.querySelector('#spouse-pob');
        this.spouseParent1Name = document.querySelector('#spouse-parent1-name');
        this.spouseParent2Name = document.querySelector('#spouse-parent2-name');
        this.marriageDate = document.querySelector('#marriage-date');
        this.location = document.querySelector('#marriage-location')
        this.descriptions = document.querySelector('.descriptions');
        this.creatorVow = document.querySelector('#creator-vow');
        this.spouseVow = document.querySelector('#spouse-vow')
        this.witnessName = document.querySelector('#witness-name');
        let certRes = await fetch(`/committed/certificate/?ID=${this.UUID}`);
        let certificate = (await certRes.json())
        if (certificate.c_first_name){
            this.creatorFName.innerHTML = certificate.c_first_name;
            this.creatorMName.innerHTML = certificate.c_middle_name;
            this.creatorLName.innerHTML = certificate.c_last_name
            this.creatorDOB.innerHTML = moment(certificate.c_date_of_birth).format('YYYY-MM-DD')
            this.creatorPOB.innerHTML = certificate.c_place_of_birth
            this.creatorParent1Name.innerHTML = certificate.c_parent1_name
            this.creatorParent2Name.innerHTML = certificate.c_parent2_name
            this.spouseFName.innerHTML = certificate.s_first_name
            this.spouseMName.innerHTML = certificate.s_middle_name
            this.spouseLName.innerHTML = certificate.s_last_name
            this.spouseDOB.innerHTML = moment(certificate.s_date_of_birth).format('YYYY-MM-DD')
            this.spousePOB.innerHTML = certificate.s_place_of_birth
            this.spouseParent1Name.innerHTML = certificate.s_parent1_name
            this.spouseParent2Name.innerHTML = certificate.s_parent2_name
            this.marriageDate.innerHTML = moment(certificate.marriage_date).format('YYYY-MM-DD')
            this.location.innerHTML = certificate.location
            this.creatorVow.innerHTML = certificate.creator_vow
            this.spouseVow.innerHTML = certificate.spouse_vow
            this.witnessName.innerHTML = certificate.witness_name
            this.descriptions.innerHTML = certificate.description
            this.certId = certificate.certificate_id
            this.photoIdMap = certificate.photoIdMap;
            this.photoIds = certificate.photoIds
            this.storyIds = certificate.storyIds;
            this.creatorId = certificate.creatorId;
            this.spouseId = certificate.spouseId;

            let canvasWidth = document.querySelector('#canvas').offsetWidth
            let canvasHeight = document.querySelector('#canvas').offsetHeight
            window.canvas.signaturePad[0].fromDataURL(certificate.e_signature_1,{width: canvasWidth, height: canvasHeight})
            window.canvas.signaturePad[1].fromDataURL(certificate.e_signature_2,{width: canvasWidth, height: canvasHeight})
            window.canvas.signaturePad[2].fromDataURL(certificate.e_signature_witness,{width: canvasWidth,height: canvasHeight})
            console.log(certificate)
            // console.log(this.storyIds)
            // console.log(certificate.photo)

            window.displayMap.initMap({lat: parseFloat(certificate.latitude), lng: parseFloat(certificate.longitude)}, certificate.place_id)
            
            certificate.photo.map(x=>window.dragAndDrop.filesUrls.push(x))
            if (window.dragAndDrop.filesUrls.length>0){
                window.dragAndDrop.populateCarousel(window.dragAndDrop.filesUrls, this.photoIdMap)

            }
        }
        else{
            // window.location.replace("http://www.w3schools.com");
        }

    }

}