class DisplayMap {
  constructor() {
    this.labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    this.labelIndex = 0;
    this.map;
    this.eiffel = { lat: 48.8584, lng: 2.2945 };
    this.initMap(this.eiffel);
    this.lat;
    this.lng;
  }

  initMap = async (coordinates, placeId) => {
    // instantiate an autocomplete object that takes the input from html
    if (!placeId) {
      let locationRes = await fetch("/proposal-cert/location");
      this.initialPlace = await locationRes.json();
    } else {
      let locationRes = await fetch(`/proposal-cert/location?place_id=${placeId}`);
      this.initialPlace = await locationRes.json();
    }
    // infowindow object to popup when selected
    var infowindow = new google.maps.InfoWindow();
    // display back the content to html using setContent
    let infowindowContent = document.querySelector("#infowindow-content");
    infowindow.setContent(infowindowContent);

    this.map = new google.maps.Map(document.querySelector("#map"), {
      center: coordinates,
      zoom: 15
    });
    var marker = new google.maps.Marker({
      map: this.map,
      anchorPoint: new google.maps.Point(0, -29),
      position: coordinates
    });

    let address = "";
    address = [
      (this.initialPlace.address_components[0] &&
        this.initialPlace.address_components[0].short_name) ||
        "",
      (this.initialPlace.address_components[1] &&
        this.initialPlace.address_components[1].short_name) ||
        "",
      (this.initialPlace.address_components[2] &&
        this.initialPlace.address_components[2].short_name) ||
        ""
    ].join(" ");

    let url =
      "https://maps.gstatic.com/mapfiles/place_api/icons/jewelry-71.png";

    infowindowContent.children["place-icon"].src = url;
    infowindowContent.children[
      "place-name"
    ].textContent = this.initialPlace.name;
    infowindowContent.children["place-address"].textContent = address;
    
    infowindow.open(map, marker);
    this.lat = this.initialPlace.geometry.location.lat;
    this.lng = this.initialPlace.geometry.location.lng;
    console.log(this.lat, this.lng)
  };
}
