class FormConfirmation {
  constructor() {
    this.confirmBtn = document.querySelector("#confirm-btn");
    this.confirmationModal = document.querySelector("#confirmation-modal");
    this.creatorFName = document.querySelector("#creator-fname");
    this.creatorMName = document.querySelector("#creator-mname");
    this.creatorLName = document.querySelector("#creator-lname");
    this.creatorDOB = document.querySelector("#creator-dob");
    this.creatorPOB = document.querySelector("#creator-pob");
    this.creatorParent1Name = document.querySelector("#creator-parent1-name");
    this.creatorParent2Name = document.querySelector("#creator-parent2-name");
    this.spouseFName = document.querySelector("#spouse-fname");
    this.spouseMName = document.querySelector("#spouse-mname");
    this.spouseLName = document.querySelector("#spouse-lname");
    this.spouseDOB = document.querySelector("#spouse-dob");
    this.spousePOB = document.querySelector("#spouse-pob");
    this.spouseParent1Name = document.querySelector("#spouse-parent1-name");
    this.spouseParent2Name = document.querySelector("#spouse-parent2-name");
    this.marriageDate = document.querySelector("#marriage-date");
    this.marriageLocation = document.querySelector('#marriage-location')
    this.descriptions = document.querySelector(".descriptions");
    this.creatorVow = document.querySelector("#creator-vow");
    this.spouseVow = document.querySelector("#spouse-vow");
    this.witnessName = document.querySelector("#witness-name");


    this.confirmBtn.addEventListener("click", ()=>{
        $('#confirmation-modal-container').modal({
            show: true,
            keyboard: true,
            backdrop: true, ignoreBackdropClick: true
        })
        this.populateModal(window.dragAndDrop.filesUrls, window.loadCertificate.photoIdMap)});
  }

  populateModal = (filesUrls, photoIdMap ) => {
      
    let inner = `
        <div class='row'>
            <div class='col-md-6'>
                <b>Your Information</b>
                <div>First Name: ${this.creatorFName.value}</div>
                <div>Middle Name: ${this.creatorMName.value}</div>
                <div>Last Name: ${this.creatorLName.value}</div>
                <div>Date of birth: ${this.creatorDOB.value}</div>
                <div>Place of birth: ${this.creatorPOB.value}</div>
                <div>Parent1 Name: ${this.creatorParent1Name.value}</div>
                <div>Parent2 Name: ${this.creatorParent2Name.value}</div>
            </div>
            <div class='col-md-6'>
                <b>Your Spouse</b>
                <div>First Name: ${this.spouseFName.value}</div>
                <div>Middle Name: ${this.spouseMName.value}</div>
                <div>Last Name: ${this.spouseLName.value}</div>
                <div>Date of birth: ${this.spouseDOB.value}</div>
                <div>Place of birth: ${this.spousePOB.value}</div>
                <div>Parent1 Name: ${this.spouseParent1Name.value}</div>
                <div>Parent2 Name: ${this.spouseParent2Name.value}</div>
            </div>
        </div>
        <div class='row'>
            <div class='col-md'>
                <b>Marriage Information</b>
                <div>Marriage date: ${this.marriageDate.value}</div>
                <div>Marriage location: ${this.marriageLocation.value}</div>
            </div>
        </div>
        <b>Marriage Information</b>
        <div class='row'>`;
    for (let i = 0; i < filesUrls.length; i++){
        let url;
        let photoId
        if (typeof(filesUrls[i]) == 'string'){
            url = filesUrls[i]
            photoId = photoIdMap[url]
        }
        else{
            url = URL.createObjectURL(filesUrls[i])
        }
        inner += ` 
            <div class='col-md'>
                <div class='background sort-modal-pics confirm-pics' data-photo-id='${photoId} 'data-id='${i}' style="background-image: url(../${url})"></div>
            </div>`
    }

    inner += `
    </div>
        <div class='row'>
            <div class='col-md'>
            <div>Descriptions: ${this.descriptions.value}</div>
            </div>
        </div>
    </div>`

    inner += `        
    <div class='row'>
        <div class='col-md-6'>
            <b>Your vow</b>
            <div>${this.creatorVow.value}</div>
        </div>
        <div class='col-md-6'>
            <b>Spouse's vow</b>
            <div>${this.spouseVow.value}</div>
        </div>
    </div>
    <div>Witness name: ${this.witnessName.value}</div>`

    this.confirmationModal.innerHTML = inner 

  };
}
