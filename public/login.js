function switchRegOrLogin() {
    if(document.querySelector('#title').innerText!='Register'){
        document.querySelector('#title').innerText = 'Register'
    document.querySelector('.card-body').innerHTML = `
    <form id="formBody" method="POST" action="/reg/reg" id='reg'>
    <div class="input-group form-group">
    <div class="input-group form-group">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-user"></i></span>
    </div>
    <input type="text" class="form-control" placeholder="ID/GoogleID" name="userName">
</div>
<div class="input-group form-group">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-key"></i></span>
    </div>
    <input type="password" class="form-control" placeholder="password" name="password">
</div>
<div class="input-group form-group">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-key"></i></span>
    </div>
    <input type="email" class="form-control" placeholder="Email" name="email">
</div>
<div class="input-group form-group">
    <div class="input-group-prepend">
    <span class="input-group-text"><i class="fas fa-user"></i></span>
    </div>
    <input type="text" class="form-control" placeholder="Name" name="first_name">
</div>
<div class="input-group form-group">
    <div class="input-group-prepend">
    <span class="input-group-text"><i class="fas fa-user"></i></span>
    </div>
    <input type="text" class="form-control" placeholder="Middle Name" name="middleName">
</div>
<div class="input-group form-group">
    <div class="input-group-prepend">
    <span class="input-group-text"><i class="fas fa-user"></i></span>
    </div>
    <input type="text" class="form-control" placeholder="Last Name" name="lastName">
</div>

<div class="form-group">
    <input type="submit" value="Submit" class="btn float-right login_btn ml-3">
    <div value="Register" class="btn float-right login_btn" onClick="switchRegOrLogin()">Login</div>
</div></div></form>
`

document.querySelector('.card').style.height=`500px
    `
    }
    else{
        document.querySelector('#title').innerText = 'Login'
        document.querySelector('.card-body').innerHTML = `
        <form id="formBody" method="POST" action="/login" id='login'>
        <div class="input-group form-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-user"></i></span>
            </div>
            <input type="text" class="form-control" placeholder="username" name="username">
        </div>
        <div class="input-group form-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-key"></i></span>
            </div>
            <input type="password" class="form-control" placeholder="password" name="password">
        </div>
        <div class="form-group">
            <input type="submit" value="Submit" class="btn float-right login_btn ml-3">
            <div value="Register" class="btn float-right login_btn" onClick="switchRegOrLogin()">Register</div>
        </div>
    `
    document.querySelector('.card').style.height=`300px`
    }
  }
  
