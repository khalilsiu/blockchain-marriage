function qs(name) {
  var query = window.location.search.substring(1); // Remove question mark
  console.log(query)
  var parameters = query.split('&');
  console.log(parameters)
  for (var i = 0; i < parameters.length; i++) {
      var pair = parameters[i].split('=');

      if (pair[0] == name) {
          return pair[1];
      }
  }

  return null;
}

const id=qs('ID')
console.log(id)
let  url = `/public/getCertInfoByIDPublic/?ID=${id}`;
  fetch(url, {
          method: 'GET',
      })
      .then(
          function(response) {
              response.json().then(function(data) {
                let cert = document.querySelector("#cert");
      let marriage_date=new Date(data[0]);
      console.log(marriage_date.toUTCString())
      cert.innerHTML = `<div style="height: 210mm;
      width: 148.5mm; padding:10px; text-align:center; background-color:#ffffff; font-family:Cursive;">
                <div style="height:200mm; padding:10px; text-align:center; border:5px solid #07278f">
                       <span style="font-size:25px; font-weight:bold">Certificate of Registration of Marriage</span>
                       <br/><br/>

                       <br>
                       <span style="font-size:20px"><b>${
                         data[1][1]
                       } and ${data[2][1]}</b></span><br/><br/>
                       <span style="font-size:12px"><i>were united before the World in marriage</i></span> <br/><br/>
                        <br/><span style="font-size:14px"><i>This is to Certify that ${
                          data[1][1]
                        }</i></span><br/>
                        <br/> <span style="font-size:14px"><i>residing at Hong Kong  </i></span><br/>
                        <br/> <span style="font-size:14px"><i>born on</i></span><br/>
                        <br/> <span style="font-size:14px"><i>and ${
                          data[2][1]
                        } residing at Hong Kong</i></span><br/>
                        <br/> <span style="font-size:14px"><i>born on</i></span><br/
                        <br/> <span style="font-size:14px"><i>has been solemnized on ${marriage_date}. </i></span><br/>
                        <br/><br/>
                       <span style="font-size:12px"><i>Witness by ${data[0].witness_name} <img src="${
                         data[0][5]
                       }" width="100px"></i></span><br><br><br>
                                 <span style="font-size:15px"><i>Signture<img src="${
                                   data[1].creator_sign
                                 }" width="100px"></i>
                                 <i>Signture<img src="${
                                   data[2].spouse_sign
                                 }" width="100px"></i></span>
                       <br/> <br/>
                    </div>
                </div>`;
              })

          }
      )
      .catch(function(err) {
          console.log('Fetch Error :-S', err);
      })
      
