const puppeteer = require('puppeteer');

(async () => {

    var options = {
        format: 'A4',
        headerTemplate: "<p></p>",
        footerTemplate: "<p></p>",
        displayHeaderFooter: false,
        margin: {
            top: "40px",
            bottom: "100px"
        },
        printBackground: true,
        path: 'invoice.pdf'
    }

    const browser = await puppeteer.launch({
        args: ['--no-sandbox'],
        headless: true
    });
    const page = await browser.newPage();
    await page.goto(`http://localhost:8081/public/cert/?ID=55`, {
        waitUntil: 'networkidle0'
    });
    await page.pdf(options);
    await browser.close();
})();