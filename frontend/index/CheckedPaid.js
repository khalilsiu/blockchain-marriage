class CheckPaid{
    constructor(){

    }

    checkPaid = async()=>{
        console.log(window.loadCertificate.certId)
        if (window.loadCertificate.certId){
            let paidRes = await fetch(`/paid-status/?certId=${window.loadCertificate.certId}`)
            let paid = await paidRes.json();
            return paid
        }else{
            return false
        }
    }
}