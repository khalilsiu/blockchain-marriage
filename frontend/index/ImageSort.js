
class ImageSort {
  constructor() {}

  imageSort = (filesUrls,photoIdMap) => {
    this.modalBody = document.querySelector("#index-modal");
    
    let division = Math.floor(12 / filesUrls.length);

    let inner = `
        <div class="modal-header">
            <h5 class="modal-title" id="confirmation-modal-label">Swap photos order</h5>
            <i id="return-btn" class="fa fa-pencil" aria-hidden="true"></i>
        </div>
        <div class="modal-body" id="confirmation-modal">
            <div class='row sort-row'>`;
    
      for (let i = 0; i < filesUrls.length; i++) {
        let url;
        let photoId;
        let fileId;
        if (typeof(filesUrls[i])=='string'){
          url =  filesUrls[i]
          photoId = photoIdMap[url]
        }
        else{
          url = URL.createObjectURL(filesUrls[i])
          fileId = i
        }
        inner += ` 
                  <div class='col-md-${division} sort-photos'>
                      <i class="fa fa-times remove-btn" aria-hidden="true"></i>
                      <div class='background sort-modal-pics' data-file-id = '${fileId}'data-photo-id='${photoId}' style="background-image: url(${url})"></div>
                  </div>`;
      }
    inner += `</div>
        </div>`;
    this.modalBody.innerHTML = inner;

    const sortable = new Draggable.Sortable(document.querySelectorAll(".sort-row"), {
      draggable: ".sort-photos"
    });
    sortable.on("sortable:sorted", (e) => {
      filesUrls[e.newIndex] = [filesUrls[e.oldIndex], filesUrls[e.oldIndex] = filesUrls[e.newIndex]][0];
      console.log(filesUrls)
  });
  

    document.querySelector('#confirmation-modal').addEventListener('click',e=>{
      if (e.target && e.target.matches('.remove-btn')){
        let url = e.target.nextElementSibling.style.backgroundImage.slice(4, -1).replace(/"/g, "")
        console.log(url)
        console.log(window.dragAndDrop.filesUrls)
        let index = window.dragAndDrop.filesUrls.indexOf(url);
        console.log(index)
        if (index<0){
          window.dragAndDrop.filesUrls.splice(e.target.nextElementSibling.dataset.fileId,1)
        }
        else{
          window.dragAndDrop.filesUrls.splice(index,1)

        }
        this.imageSort(window.dragAndDrop.filesUrls, photoIdMap)
      }
    })

    $(document).on('hide.bs.modal','#modal', function () {
      if (window.dragAndDrop.filesUrls.length == 0){
        this.carouselArea = document.querySelector("#carousel-area");
        let storyIds = []
      if (window.loadCertificate.storyIds){
        storyIds = window.loadCertificate.storyIds
      }
      else{
        storyIds[0] = 1
      }
        let inner = `<div id="drop-zone" data-story-id='${storyIds[0]}'>
            <div>
                <h3>Drag and Drop files here</h3>
            </div>
        </div>`
            this.carouselArea.innerHTML = inner;
            this.dropZone = document.querySelector("#drop-zone");
            this.dropZone.addEventListener("drop", event => window.dragAndDrop.dropHandler(event));
            this.dropZone.addEventListener("dragover", event =>
            window.dragAndDrop.dragOverHandler(event)
    );
    this.carouselArea = document.querySelector("#carousel-area");
    this.filesUrls = [];
      }
      else{
        window.dragAndDrop.populateCarousel(filesUrls, photoIdMap)
      }
    })

    this.removeBtn = document.querySelectorAll('.remove-btn');
    
    this.returnBtn = document.querySelector('#return-btn');
    this.returnBtn.addEventListener('click',()=>{
      this.repopulateModal(filesUrls, photoIdMap)})

    
  };

  repopulateModal = (filesUrls, photoIdMap)=>{
    let fileUrls = filesUrls;
    let firstObjectURL;
    let photoId;
    if (typeof(fileUrls[0])=='string'){
      firstObjectURL = fileUrls[0]
      photoId = photoIdMap[firstObjectURL]
      console.log(photoId)
    }
    else{
      firstObjectURL = URL.createObjectURL(fileUrls[0])
    }
    // let url = target.style.backgroundImage.slice(4, -1).replace(/"/g, "");
    let inner = `
    <div class="modal-body mb-0 p-0">
      <i id="close-btn" class="fa fa-times" aria-hidden="true" data-dismiss="modal"></i>
      <i id="edit-btn" class="fa fa-pencil" aria-hidden="true"></i>
      <div id="modal-carousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#modal-carousel" data-slide-to="0" class="active"></li>`;

    
    if (fileUrls && fileUrls.length > 1) {
      for (let i = 1; i < fileUrls.length; i++) {
        inner += `
          <li data-target="#modal-carousel" data-slide-to="${i}"></li>`;
      }
    }
    inner += `
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <div class='background modal-pics' data-photo-id='${photoId}' data-id='0'style="background-image: url(${firstObjectURL})"></div>
          </div>`;

          console.log(photoId)

    if (fileUrls && fileUrls.length > 1) {
      for (let i = 1; i < fileUrls.length; i++) {
        let objectURL;
        let photoId
        if (typeof(fileUrls[i])=='string'){
          objectURL = fileUrls[i]
          photoId = photoIdMap[objectURL]
        } 
        else{
          objectURL = URL.createObjectURL(fileUrls[i])
        }
        inner += `
          <div class="carousel-item">
              <div class='background modal-pics' data-photo-id='${photoId}' data-id='${i}' style="background-image: url(${objectURL})"></div>
          </div>`;
      }
    }
    inner += `
        </div>
        <a class="carousel-control-prev" href="#modal-carousel" role="button" data-slide="prev">
          <div class='arrow-container'>    
            <span><i class="fa fa-angle-left arrows" aria-hidden="true"></i></span>
          </div>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#modal-carousel" role="button" data-slide="next">
            <div class='arrow-container'>    
              <span><i class="fa fa-angle-right arrows" aria-hidden="true"></i></span>
            </div>
            <span class="sr-only">Next</span>
        </a>
      </div>
    </div>`;


    this.modalBody.innerHTML = inner;


    $(document).on('hide.bs.modal','#modal', function () {
      window.dragAndDrop.populateCarousel(filesUrls, photoIdMap)
    })

    this.editBtn = document.querySelector('#edit-btn');
    this.editBtn.addEventListener('click', ()=>{
      window.imageSort.imageSort(filesUrls, photoIdMap)});


  }
}
