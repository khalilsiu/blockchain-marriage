
window.displayMap = new DisplayMap();
window.dragAndDrop = new DragAndDrop();
window.formOperation = new FormOperation();
window.formConfirmation = new FormConfirmation();
window.canvas = new Canvas();
window.imageSort = new ImageSort();
window.loadCertificate = new LoadCertificate();
window.checkPaid = new CheckPaid();


function confirmPopUp() {
    var popup = document.getElementById("myPopup");
    popup.classList.toggle("show");
  }
