class FormOperation {
  constructor() {
    this.form = document.querySelector("#certificate");
    this.form.addEventListener("submit", event => this.preventSubmit(event));
    this.confirmBtn = document.querySelector("#confirm-btn");
    this.confirmBtn.addEventListener("click", (e) => this.preventConfirm(e));
    this.submitBtn = document.querySelector("#submit-btn");
    this.submitBtn.addEventListener("click", () => this.certSubmit());
    this.creatorFName = document.querySelector("#creator-fname");
    this.creatorMName = document.querySelector("#creator-mname");
    this.creatorLName = document.querySelector("#creator-lname");
    this.creatorDOB = document.querySelector("#creator-dob");
    this.creatorPOB = document.querySelector("#creator-pob");
    this.creatorParent1Name = document.querySelector("#creator-parent1-name");
    this.creatorParent2Name = document.querySelector("#creator-parent2-name");
    this.spouseFName = document.querySelector("#spouse-fname");
    this.spouseMName = document.querySelector("#spouse-mname");
    this.spouseLName = document.querySelector("#spouse-lname");
    this.spouseDOB = document.querySelector("#spouse-dob");
    this.spousePOB = document.querySelector("#spouse-pob");
    this.spouseParent1Name = document.querySelector("#spouse-parent1-name");
    this.spouseParent2Name = document.querySelector("#spouse-parent2-name");
    this.marriageDate = document.querySelector("#marriage-date");
    this.marriageLocation = document.querySelector("#marriage-location");
    this.descriptions = document.querySelector(".descriptions");
    this.creatorVow = document.querySelector("#creator-vow");
    this.spouseVow = document.querySelector("#spouse-vow");
    this.witnessName = document.querySelector("#witness-name");
    
    
  }

  preventConfirm = e =>{
    var characterCode
    console.log('hello',e.charCode)
    if(e.key!=undefined){
      characterCode = charCodeArr[e.key] || e.key.charCodeAt(0);
      e.preventDefault()
  }else{
      /* As @Leonid suggeted   */
      characterCode = e.which || e.charCode || e.keyCode || 0;
      e.preventDefault()
    }
  console.log(characterCode)

  }

  preventSubmit = event => {
    event.preventDefault();
  };

  certSubmit = async () => {
    this.confirmPicsOrder = document.querySelectorAll('.confirm-pics');
    this.storyOrder = document.querySelectorAll('#drop-zone')
    this.marriageLocationLat = window.displayMap.lat;
    this.marriageLocationLng = window.displayMap.lng;
    this.placeId = window.displayMap.placeId;
    this.creatorSign = window.canvas.signaturePad[0].toDataURL();
    this.spouseSign = window.canvas.signaturePad[1].toDataURL();
    this.witnessSign = window.canvas.signaturePad[2].toDataURL();
    let ids = Array.from(this.confirmPicsOrder).map(x=>x.dataset.photoId)
    let storyIds = Array.from(this.storyOrder).map(x=>x.dataset.storyId);
    console.log(storyIds);
    ids = ids.map(x=>parseInt(x.trim()))
    storyIds = storyIds.map(x=>parseInt(x.trim()));
    console.log(storyIds);
    console.log(ids)

    let creatorRes = await fetch("/user/creator", {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        creatorFName: this.creatorFName.value,
        creatorMName: this.creatorMName.value,
        creatorLName: this.creatorLName.value,
        creatorDOB: this.creatorDOB.value,
        creatorPOB: this.creatorPOB.value,
        creatorParent1Name: this.creatorParent1Name.value,
        creatorParent2Name: this.creatorParent2Name.value
      })
    });
    let creatorId = await creatorRes.json();
    console.log(creatorId);

    let spouseRes = await fetch("/user/spouse", {
      method: window.loadCertificate.certId ? "PUT" : "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        spouseFName: this.spouseFName.value,
        spouseMName: this.spouseMName.value,
        spouseLName: this.spouseLName.value,
        spouseDOB: this.spouseDOB.value,
        spousePOB: this.spousePOB.value,
        spouseParent1Name: this.spouseParent1Name.value,
        spouseParent2Name: this.spouseParent2Name.value
      })
    });
    let spouseId = await spouseRes.json();

    let certRes = await fetch("/certificate", {
      method: window.loadCertificate.certId ? "PUT" : "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        certId: window.loadCertificate.certId,
        spouseId: spouseId,
        marriageLocationLat: this.marriageLocationLat,
        marriageLocationLng: this.marriageLocationLng,
        marriageDate: this.marriageDate.value,
        marriageLocation: this.marriageLocation.value,
        witnessName: this.witnessName.value,
        creatorVow: this.creatorVow.value,
        spouseVow: this.spouseVow.value,
        creatorSign: this.creatorSign,
        spouseSign: this.spouseSign,
        witnessSign: this.witnessSign
      })
    });

    let certId = await certRes.json();
    console.log(certId);

   

    // you have to apend the file object to formData one by one
    // in multer you would need to use upload.array('photo')
    // the parameter is the field name and should be equal to the one you enter here as key


    let storyRes = await fetch("/uploads/story", {
      method: window.loadCertificate.certId ? "PUT" : "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        storyIds: storyIds,
        certId: certId,
        description: this.descriptions.value,
        location: this.marriageLocation.value,
        placeId: this.placeId,
        locationLat: this.marriageLocationLat,
        locationLng: this.marriageLocationLng
      })
    });

    let updatedStoryIds =( await storyRes.json())[0];
    console.log(updatedStoryIds)


    let formData = new FormData();
    let order = []
    if (ids){
        formData.append('ids',JSON.stringify(ids));
        formData.append('originalIds', JSON.stringify(window.loadCertificate.photoIds));
    }
    for (let i = 0; i < window.dragAndDrop.filesUrls.length; i++) {
      if (typeof (window.dragAndDrop.filesUrls[i]) == "string") {
        
    } else {
        formData.append("photo", window.dragAndDrop.filesUrls[i]);
        order.push(i);
      }
    }
    console.log(order)
    formData.append('order', JSON.stringify(order))
    console.log(updatedStoryIds)
    formData.append('storyIds',JSON.stringify(updatedStoryIds))

    let photoRes = await fetch('/uploads/photo',{
        method: window.loadCertificate.certId ? "PUT" : "POST",
        body: formData
    })
    let success = await photoRes.json();
    console.log(success)

    alert('Information is saved successfully.')
    this.paymentBtnContainer = document.querySelector('#payment-btn-container');

    let checkPaid = await window.checkPaid.checkPaid();
    console.log(checkPaid)

    if (!checkPaid){
      this.paymentBtnContainer.innerHTML = `<button id='payment-btn' type="button" class="btn btn-primary">Proceed to payment</button>`
      this.paymentBtnContainer.addEventListener('click',()=>{
        window.location.href = 'frontend/payment/payment.html'
      })
    }

  };
}
