class FormConfirmation {
  constructor() {
    this.confirmBtn = document.querySelector("#confirm-btn");
    this.confirmationModal = document.querySelector("#confirmation-modal");
    this.creatorFName = document.querySelector("#creator-fname");
    this.creatorMName = document.querySelector("#creator-mname");
    this.creatorLName = document.querySelector("#creator-lname");
    this.creatorDOB = document.querySelector("#creator-dob");
    this.creatorPOB = document.querySelector("#creator-pob");
    this.creatorParent1Name = document.querySelector("#creator-parent1-name");
    this.creatorParent2Name = document.querySelector("#creator-parent2-name");
    this.spouseFName = document.querySelector("#spouse-fname");
    this.spouseMName = document.querySelector("#spouse-mname");
    this.spouseLName = document.querySelector("#spouse-lname");
    this.spouseDOB = document.querySelector("#spouse-dob");
    this.spousePOB = document.querySelector("#spouse-pob");
    this.spouseParent1Name = document.querySelector("#spouse-parent1-name");
    this.spouseParent2Name = document.querySelector("#spouse-parent2-name");
    this.marriageDate = document.querySelector("#marriage-date");
    this.marriageLocation = document.querySelector('#marriage-location')
    this.descriptions = document.querySelector(".descriptions");
    this.creatorVow = document.querySelector("#creator-vow");
    this.spouseVow = document.querySelector("#spouse-vow");
    this.witnessName = document.querySelector("#witness-name");


    this.confirmBtn.addEventListener("click", ()=>{
        this.populateModal(window.dragAndDrop.filesUrls, window.loadCertificate.photoIdMap)});
  }

  populateModal = (filesUrls, photoIdMap ) => {
      console.log(this.creatorPOB.value)
    let inner = `
        <div class='row line-rows'>
            <div class='col-md-6'>
                <b class='big-titles'>Your Information</b>
                <div class='titles'>First name: <span class='values'>${this.creatorFName.value} </span></div>
                <div class='titles'>Middle name: <span class='values'>${this.creatorMName.value} </span></div>
                <div class='titles'>Last name: <span class='values'>${this.creatorLName.value} </span></div>
                <div class='titles'>Date of birth: <span class='values'>${this.creatorDOB.value} </span></div>
                <div class='titles'>Place of birth: <span class='values'>${this.creatorPOB.value} </span></div>
                <div class='titles'>Parent1 name: <span class='values'>${this.creatorParent1Name.value} </span></div>
                <div class='titles'>Parent2 name: <span class='values'>${this.creatorParent2Name.value} </span></div>
            </div>
            <div class='col-md-6'>
                <b class='big-titles'>And</b>
                <div class='titles'>First name: <span class='values'>${this.spouseFName.value} </span></div>
                <div class='titles'>Middle name: <span class='values'>${this.spouseMName.value} </span></div>
                <div class='titles'>Last name: <span class='values'>${this.spouseLName.value} </span></div>
                <div class='titles'>Date of birth: <span class='values'>${this.spouseDOB.value} </span></div>
                <div class='titles'>Place of birth: <span class='values'>${this.spousePOB.value} </span></div>
                <div class='titles'>Parent1 name: <span class='values'>${this.spouseParent1Name.value} </span></div>
                <div class='titles'>Parent2 name: <span class='values'>${this.spouseParent2Name.value} </span></div>
            </div>
        </div>
        <div class='section-separator'></div>
        <div class='row line-rows'>
            <div class='col-md'>
                <b class='big-titles'>Marriage Details</b>
                <div class='titles'>Marriage date: <span class='values'>${this.marriageDate.value} </span></div>
                <div class='titles'>Marriage location: <span class='values'>${this.marriageLocation.value} </span></div>
            </div>
        </div>
        <b class='big-titles'>Memories</b>
        <div class='row line-rows'>`;
    for (let i = 0; i < filesUrls.length; i++){
        let url;
        let photoId
        if (typeof(filesUrls[i]) == 'string'){
            url = filesUrls[i]
            photoId = photoIdMap[url]
        }
        else{
            url = URL.createObjectURL(filesUrls[i])
        }
        inner += ` 
            <div class='col-md'>
                <div class='background sort-modal-pics confirm-pics' data-photo-id='${photoId} 'data-id='${i}' style="background-image: url(${url})"></div>
            </div>`
    }

    inner += `
    </div>
        <div class='row line-rows'>
            <div class='col-md'>
            <div class='titles'>Descriptions: <span class='values'>${this.descriptions.value} </span></div>
            </div>
        </div>
    </div>`

    inner += `        
    <div class='row line-rows'>
        <div class='col-md-6'>
            <div class='titles'>Your vow</div>
            <div class='values'>${this.creatorVow.value}</div>
 
        </div>
        <div class='col-md-6'>
            <div class='titles'>Your vow</div>
            <div class='values'>${this.spouseVow.value}</div>
        </div>
    </div>
    <div class='titles'>Witness name: <span class='values'>${this.witnessName.value} </span></div>`

    this.confirmationModal.innerHTML = inner 

  };

  
}
