let url = "/getData/getCertInfo";
fetch(url, {
  method: "GET"
})
  .then(function(response) {
    response.json().then(function(data) {
      console.log(data)
      let cert = document.querySelector("#cert");
      let marriage_date=new Date(data[0]);
      console.log(marriage_date.toUTCString())
      cert.innerHTML = `<div style="height: 210mm;
      width: 148.5mm; padding:10px; text-align:center; background-color:#ffffff; font-family:Cursive;">
                <div style="height:200mm; padding:10px; text-align:center; border:5px solid #07278f">
                       <span style="font-size:25px; font-weight:bold" class="mt-5">Certificate of Registration of Marriage</span>
                       <br/><br/>

                       <br>
                       <span style="font-size:20px"><b>${
                         data[1][1]
                       } and ${data[2][1]}</b></span><br/><br/>
                       <span style="font-size:20px"><i>were united before the World in marriage</i></span> <br/><br/>
                        <span style="font-size:18px"><i>This is to Certify that ${
                          data[1][1]
                        }</i></span><br/>
                        <br/> <span style="font-size:18px"><i>residing at Hong Kong  </i></span><br/>
                        <br/> <span style="font-size:18px"><i>born on</i></span><br/>
                        <br/> <span style="font-size:18px"><i>and ${
                          data[2][1]
                        } residing at Hong Kong</i></span><br/>
                        <br/> <span style="font-size:18px"><i>born on</i></span><br/
                        <br/> <span style="font-size:18px"><i>has been solemnized on ${marriage_date}. </i></span><br/>
                        <br/><br/>
                       <span style="font-size:12px"><i>Witness by ${data[0].witness_name} <img src="${
                         data[0][5]
                       }" width="200px"></i></span><br><br><br>
                                 <span style="font-size:8px"><i>Signture<img src="${
                                   data[1].creator_sign
                                 }" width="200px"></i>
                                 <i>Signture<img src="${
                                   data[2].spouse_sign
                                 }" width="200px"></i></span>
                       <br/> <br/>
                    </div>
                </div>`;
    });
  })
  .catch(function(err) {
    console.log("Fetch Error :-S", err);
  });

let url2 = "/user/getSelfID";
fetch(url2, {
  method: "GET"
})
  .then(function(response) {
    response.json().then(function(data) {
      console.log(data);
      let userID = data.id;
      document.querySelector("#shareLink").innerHTML = `
                   <div class="input-group mb-3 mt-3">
                   Your Certificate URL:<span class="input-group-text ml-2" id="basic-addon3"> http://app.pontdesartsweb.info/public/cert/?ID=${userID}</span></div>`;

    
    });
  })
  .catch(function(err) {
    console.log("Fetch Error :-S", err);
  });
