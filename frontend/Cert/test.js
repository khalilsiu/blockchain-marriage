var request = require('request');
var fs = require('fs');

var options = { method: 'POST',
  url: 'https://restpack.io/api/html2pdf/v6/convert',
  headers: { 'x-access-token': 'A79xWAp5EOd9In9p0duKjoCSQ8sPez6hjdorvQRloiB1AoTY' },
  json: true,
  form: {
    url: 'http://localhost:8081/user/payment/',
    json: true
  }
};

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
  
  fs.writeFile('out.pdf', body, function() {
    console.log("PDF Saved.");
  /*
    {
      "cached": true,
      "height": 768,
      "image": "https://cdn.restpack.io/a/cache/pdf/96f6cd877429e5b37e96533cff96c52ff10fc687d537d6cad72f1f2795c020ec",
      "remote_status": 200,
      "run_time": 1166,
      "url": "https://google.com",
      "width": 1280
    }
  */

})})
