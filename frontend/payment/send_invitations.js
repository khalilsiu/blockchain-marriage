let  url = '/user/sendUUID';
  fetch(url, {
          method: 'GET',
      })
      .then(
          function(response) {
              console.log(response)
              response.json().then(function(data) {
                  console.log(data)
                  document.querySelector('#informationKey').innerHTML
                  =`<div class="my-3">Or if you wish to proceed with completing the other half
                  </br> You may press this link: </br> <a href="http://app.pontdesartsweb.info/proposal/?ID=${data.UUID}">http://app.pontdesartsweb.info/proposal/?ID=${data.UUID}</a></div>`
              })

          }
      )
      .catch(function(err) {
          console.log('Fetch Error :-S', err);
      })
      
      var words = document.getElementsByClassName('word');
      var wordArray = [];
      var currentWord = 0;
      
      words[currentWord].style.opacity = 1;
      
      // разбиваем каждле слово на буквы и помещяем каждую букву в span
      for (var  i = 0;  i < words.length; i++) {
        splitLetters(words[i]);
      }
      
      //функция смены слова
      function changeWord() {
        var oldWord = wordArray[currentWord];
        var newWord = currentWord == words.length - 1 ? wordArray[0]: wordArray[currentWord + 1];
        //цикл вызова функции для прятания буквы
        for (var i = 0; i < oldWord.length; i++) {
          letterOut(oldWord, i);
        }
        //цикл вызова функции для появления буквы
        for (var i = 0; i < newWord.length; i++) {
          newWord[i].className = 'letter behind';
          newWord[0].parentElement.style.opacity = 1;
          letterIn(newWord, i);
        }
        
        currentWord = (currentWord == wordArray.length-1) ? 0 : currentWord + 1;
      }
      
      function letterOut (oldWord, i) {
        setTimeout(function() {
              oldWord[i].className = 'letter out';
        }, i*80);
      }
      
      function letterIn (newWord, i) {
        setTimeout(function() {
              newWord[i].className = 'letter in';
        }, 340+(i*80));
      }
      
      // функция разбития слова на буквы
      function splitLetters (word) {
        var wordContent = word.innerHTML;
        var letters = [];
        word.innerHTML = '';
        
        for (var i = 0; i < wordContent.length; i++) {
          var letter = document.createElement('span');
          letter.className = 'letter';
          letter.innerHTML = wordContent[i];
          word.appendChild(letter);
          letters.push(letter);
        }
        
        wordArray.push(letters);
      }
      changeWord();
      setInterval(changeWord, 4000);