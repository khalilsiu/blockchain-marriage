var stripe = Stripe("pk_test_Vgvfy9Qi1hOdfmdUK37uAkU700dhimmHvm");

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: "#ffffff",
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: "antialiased",
    fontSize: "16px",
    "::placeholder": {
      color: "#aab7c4"
    }
  },
  invalid: {
    color: "#fa755a",
    iconColor: "#fa755a"
  }
};

// Create an instance of the card Element.
var card = elements.create("card", { style: style });

// Add an instance of the card Element into the `card-element` <div>.
card.mount("#card-element");

// Handle real-time validation errors from the card Element.
card.addEventListener("change", function(event) {
  var displayError = document.getElementById("card-errors");
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = "";
  }
});

// Handle form submission.
var form = document.getElementById("payment-form");
form.addEventListener("submit", function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById("card-errors");
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById("payment-form");
  var hiddenInput = document.createElement("input");
  hiddenInput.setAttribute("type", "hidden");
  hiddenInput.setAttribute("name", "stripeToken");
  hiddenInput.setAttribute("value", token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}

document
  .querySelector("#passwordForm")
  .addEventListener("submit", function(event) {
    event.preventDefault();
    const form = this;
    const formData = {};
    for (let input of form) {
      if (!["submit", "reset"].includes(input.type)) {
        formData[input.name] = input.value;
      }
    }
    postData("/user/password/edit", formData)
      .then(data => console.log(JSON.stringify(data))) // JSON-string from `response.json()` call
      .catch(error => console.error(error));
  });

function postData(url = "", data = {}) {
  // Default options are marked with *
  return fetch(url, {
    method: "POST", // *GET, POST, PUT, DELETE, etc.
    mode: "cors", // no-cors, cors, *same-origin
    cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
    credentials: "same-origin", // include, *same-origin, omit
    headers: {
      "Content-Type": "application/json"
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: "follow", // manual, *follow, error
    referrer: "no-referrer", // no-referrer, *client
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  }).then(response => response.json()); // parses JSON response into native JavaScript objects
}
function confirmPopUp() {
  var popup = document.getElementById("myPopup");
  popup.classList.toggle("show");
}

(async () => {
  let contentContainer = document.querySelector("#content-container");
  let certRes = await fetch("/certificate");
  let certificate = await certRes.json();
  console.log(certificate);
  if (certificate.c_first_name) {
    // creatorVow.innerText = certificate.creator_vow;
    // spouseVow.innerText = certificate.spouse_vow;
    // witnessName.innerText = certificate.witness_name;
    // descriptions.innerText = certificate.description;
    let inner = `<div class='row line-rows'>
    <div class='col-md-6'>
        <b class='big-titles'>Your Information</b>
        <div class='titles'>First name: <span class='values'>${
          certificate.c_first_name
        } </span></div>
        <div class='titles'>Middle name: <span class='values'>${
          certificate.c_middle_name
        } </span></div>
        <div class='titles'>Last name: <span class='values'>${
          certificate.c_last_name
        } </span></div>
        <div class='titles'>Date of birth: <span class='values'>${moment(
          certificate.c_date_of_birth
        ).format("YYYY-MM-DD")} </span></div>
        <div class='titles'>Place of birth: <span class='values'>${
          certificate.c_place_of_birth
        } </span></div>
        <div class='titles'>Parent1 name: <span class='values'>${
          certificate.c_parent1_name
        } </span></div>
        <div class='titles'>Parent2 name: <span class='values'>${
          certificate.c_parent2_name
        } </span></div>
    </div>
    <div class='col-md-6'>
        <b class='big-titles'>And</b>
        <div class='titles'>First name: <span class='values'>${
          certificate.s_first_name
        } </span></div>
        <div class='titles'>Middle name: <span class='values'>${
          certificate.s_middle_name
        } </span></div>
        <div class='titles'>Last name: <span class='values'>${
          certificate.s_last_name
        } </span></div>
        <div class='titles'>Date of birth: <span class='values'>${moment(
          certificate.s_date_of_birth
        ).format("YYYY-MM-DD")} </span></div>
        <div class='titles'>Place of birth: <span class='values'>${
          certificate.s_place_of_birth
        } </span></div>
        <div class='titles'>Parent1 name: <span class='values'>${
          certificate.s_parent1_name
        } </span></div>
        <div class='titles'>Parent2 name: <span class='values'>${
          certificate.s_parent2_name
        } </span></div>
    </div>
</div>
<div class='section-separator'></div>
<div class='row line-rows'>
    <div class='col-md'>
        <b class='big-titles'>Marriage Details</b>
        <div class='titles'>Marriage date: <span class='values'>${moment(
          certificate.marriage_date
        ).format("YYYY-MM-DD")} </span>
        </div>
        <div class='titles'>Marriage location: <span class='values'>${
          certificate.location
        } </span>
        </div>
    </div>
</div>
<div class='row line-rows'>`
for (let i = 0; i < certificate.photo.length; i++){
  let url;
  let photoId
  if (typeof(certificate.photo[i]) == 'string'){
      url = certificate.photo[i]
  }
  else{
      url = URL.createObjectURL(filesUrls[i])
  }
  inner += ` 
      <div class='col-md'>
          <div class='background sort-modal-pics confirm-pics' style="background-image: url(../../${url})"></div>
      </div>`
}

inner += 
`
</div>
<div class='row line-rows'>
  <div class='col-md'>
    <div class='titles'>Do you remember?</div>
    <p class='values'>${
      certificate.description
    }</p>
  </div>
</div>`;
        console.log(certificate.photo)
    contentContainer.innerHTML = inner;
  }
})();
