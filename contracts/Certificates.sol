pragma solidity ^0.5.0;

// this contract name has to be the same as the file name to be found in the deploy js file
contract Certificates {

    uint public certificateCount;
    mapping(uint => Certificate) public certificates;
    mapping(uint => CreatorInfo) public creators;
    mapping(uint => SpouseInfo) public spouses;

    struct Certificate{
        uint id;
        uint marriage_date;
        int marriage_location_lat;
        int marriage_location_lon;
        string witness_name;
        string witness_sign;
        uint creation_date;
    }

    struct CreatorInfo {
        uint id;
        string creator_name;
        uint creator_DOB;
        string creator_POB;
        string creator_f_name;
        string creator_m_name;
        string creator_vow;
        string creator_sign;
    }

    struct SpouseInfo {
        uint id;
        string spouse_name;
        uint spouse_DOB;
        string spouse_POB;
        string spouse_f_name;
        string spouse_m_name;
        string spouse_vow;
        string spouse_sign;

    }

    constructor() public{
        
    }

    function addCertificate(
        uint _marriage_date,
        int _marriage_location_lat,
        int _marriage_location_lon,
        string memory _witness_name,
        string memory _witness_sign,
        uint _creation_date
    ) public
    {
        certificateCount++;
        certificates[certificateCount] = Certificate(
            certificateCount,
            _marriage_date,
            _marriage_location_lat,
            _marriage_location_lon,
            _witness_name,
            _witness_sign,
            _creation_date
        );
    }

    function addCreator(
        string memory _creator_name,
        uint _creator_DOB,
        string memory _creator_POB,
        string memory _creator_f_name,
        string memory _creator_m_name,
        string memory _creator_vow,
        string memory _creator_sign
    ) public {
        creators[certificateCount] = CreatorInfo(
            certificateCount,
            _creator_name,
            _creator_DOB,
            _creator_POB,
            _creator_f_name,
            _creator_m_name,
            _creator_vow,
            _creator_sign);
    }

    function addSpouse(
        string memory _spouse_name,
        uint _spouse_DOB,
        string memory _spouse_POB,
        string memory _spouse_f_name,
        string memory _spouse_m_name,
        string memory _spouse_vow,
        string memory _spouse_sign
    ) public {
        spouses[certificateCount] = SpouseInfo(
            certificateCount,
            _spouse_name,
            _spouse_DOB,
            _spouse_POB,
            _spouse_f_name,
            _spouse_m_name,
            _spouse_vow,
            _spouse_sign);
    }




}