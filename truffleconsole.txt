Certificates.deployed().then(x=>app=x)
app.addCertificate(1563787643746,488584,22945,'Peter','ieieie',1563787643746)
app.certificates(1)
app.addCreator('darren',1563787643746,'Hong Kong','fanik','effie','i love you','1asdeesdaf')
app.creators(1)
app.addSpouse('thisby',1563787643746,'Hong Kong','popo','pepe','i dont love you','oihwern')

let Web3 = require('web3')
let url = 'https://mainnet.infura.io/v3/abb846960276472194c1db0fb81d9baa'
let web3 = new Web3(url)
let address = '0xBE0eB53F46cd790Cd13851d5EFf43D12404d33E8'
web3.eth.getBalance((address,(err,bal)=>{balance = bal}))
web3.utils.fromWei(balance,'ether')

you can use these to get balance from ganache account

Interact with existing contract
new web3.eth.Contract(jsonInterface, address, options)

jsonInterface - is a JSON file that says what methods can be run about the file e.g. tokens
address - is the contract address
contract.methods to see the methods
contract.methods.name().call().then(x=> name=x) > name 
contract.methods.balanceOf(address).call().then(x=> name=x)

> const account1 = '0x0E19baE250a9218EcF2e61995E9f2B7D263a6a20'
> const account2 = '0x4f8DA51EA4131572AB71D9bB8a8EC099b8501909'
> web3.eth.sendTransaction({from: account1, to: account2, value: web3.utils.toWei('1','ether')}) > pay from one account to another

web3.eth.accounts.create()