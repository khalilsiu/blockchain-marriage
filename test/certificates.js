
let assert = require('assert');
let Certificate = artifacts.require('./Certificates.sol')

contract('Certificate',async (accounts)=>{

    let certificateInstance;
    beforeEach(async()=>{
        certificateInstance = await Certificate.new();
    })

    it('returns 1 certificate after adding 1',async ()=>{
        await certificateInstance.addCertificate(1563787643746,488584,22945,'Peter','ieieie',1563787643746);
        let certCount = await certificateInstance.certificateCount();
        assert.equal(certCount,1, 'a certificate is incremented');
        let date = (await certificateInstance.certificates(1))[1].toNumber();
        assert.equal(date, 1563787643746, 'date is correctly displayed')
        let latitude = (await certificateInstance.certificates(1))[2].toNumber();
        assert.equal(latitude, 488584, 'latitude is correctly displayed')
        let longitude = (await certificateInstance.certificates(1))[3].toNumber();
        assert.equal(longitude, 22945, 'latitude is correctly displayed')
        let witnessName = (await certificateInstance.certificates(1))[4];
        assert.equal(witnessName, 'Peter', 'witness name is correctly displayed')
        let witnessSign = (await certificateInstance.certificates(1))[5];
        assert.equal(witnessSign, 'ieieie', 'sign is correctly displayed')
        
    })

    it('returns creator information correctly', async()=>{
        await certificateInstance.addCertificate(1563787643746,488584,22945,'Peter','ieieie',1563787643746);
        await certificateInstance.addCreator('darren',1563787643746,'Hong Kong','fanik','effie','i love you','1asdeesdaf')
        let certCount = await certificateInstance.certificateCount();
        let creator_id = (await certificateInstance.creators(1))[0].toNumber();
        assert.equal(certCount,creator_id, 'certificate count should be equal to creator id');
        let pob = (await certificateInstance.creators(1))[3]
        assert.equal(pob, 'Hong Kong','place of birth is correctly displayed');        
    })

    it('returns spouse information correctly', async()=>{
        await certificateInstance.addCertificate(1563787643746,488584,22945,'Peter','ieieie',1563787643746);
        await certificateInstance.addCreator('darren',1563787643746,'Hong Kong','fanik','effie','i love you','1asdeesdaf')
        await certificateInstance.addSpouse('thisby',1563787643746,'United States','popo','pepe','i dont love you','oihwern')
        let certCount = await certificateInstance.certificateCount();
        let creator_id = (await certificateInstance.creators(1))[0].toNumber();
        assert.equal(certCount,creator_id, 'certificate count should be equal to creator id');
        let creatorPob = (await certificateInstance.creators(1))[3]
        assert.equal(creatorPob, 'Hong Kong','place of birth of creator is correctly displayed'); 
        let spousePob = (await certificateInstance.spouses(1))[3]
        assert.equal(spousePob, 'United States','place of birth of spouse is correctly displayed');          
    })





})