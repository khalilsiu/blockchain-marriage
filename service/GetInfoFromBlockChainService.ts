import * as Knex from "knex";

export class GetInfoFromBlockChainService {
  constructor(private knex: Knex) {}

  public async getInfoFromBlockChain(commited_cert_number: number) {
    const Web3 = require("web3");
    const web3 = new Web3(
      "https://ropsten.infura.io/v3/726e035b15274e5bb67aedee4e9f4e1e"
    );
    const privateKey =
      "06B49DDCFC13CDE20CF428F55DC7E53CE6A8A4A298CC3618FF7F55153C655F1F";
    const account = web3.eth.accounts.privateKeyToAccount("0x" + privateKey);
    web3.eth.accounts.wallet.add(account);
    web3.eth.defaultAccount = account.address;
    console.log(web3.eth.defaultAccount);

    const contractABI = [
      {
        constant: true,
        inputs: [{ name: "", type: "uint256" }],
        name: "spouses",
        outputs: [
          { name: "id", type: "uint256" },
          { name: "spouse_name", type: "string" },
          { name: "spouse_DOB", type: "uint256" },
          { name: "spouse_POB", type: "string" },
          { name: "spouse_f_name", type: "string" },
          { name: "spouse_m_name", type: "string" },
          { name: "spouse_vow", type: "string" },
          { name: "spouse_sign", type: "string" }
        ],
        payable: false,
        stateMutability: "view",
        type: "function"
      },
      {
        constant: false,
        inputs: [
          { name: "_creator_name", type: "string" },
          { name: "_creator_DOB", type: "uint256" },
          { name: "_creator_POB", type: "string" },
          { name: "_creator_f_name", type: "string" },
          { name: "_creator_m_name", type: "string" },
          { name: "_creator_vow", type: "string" },
          { name: "_creator_sign", type: "string" }
        ],
        name: "addCreator",
        outputs: [],
        payable: false,
        stateMutability: "nonpayable",
        type: "function"
      },
      {
        constant: true,
        inputs: [],
        name: "certificateCount",
        outputs: [{ name: "", type: "uint256" }],
        payable: false,
        stateMutability: "view",
        type: "function"
      },
      {
        constant: true,
        inputs: [{ name: "", type: "uint256" }],
        name: "certificates",
        outputs: [
          { name: "id", type: "uint256" },
          { name: "marriage_date", type: "uint256" },
          { name: "marriage_location_lat", type: "int256" },
          { name: "marriage_location_lon", type: "int256" },
          { name: "witness_name", type: "string" },
          { name: "witness_sign", type: "string" },
          { name: "creation_date", type: "uint256" }
        ],
        payable: false,
        stateMutability: "view",
        type: "function"
      },
      {
        constant: false,
        inputs: [
          { name: "_marriage_date", type: "uint256" },
          { name: "_marriage_location_lat", type: "int256" },
          { name: "_marriage_location_lon", type: "int256" },
          { name: "_witness_name", type: "string" },
          { name: "_witness_sign", type: "string" },
          { name: "_creation_date", type: "uint256" }
        ],
        name: "addCertificate",
        outputs: [],
        payable: false,
        stateMutability: "nonpayable",
        type: "function"
      },
      {
        constant: false,
        inputs: [
          { name: "_spouse_name", type: "string" },
          { name: "_spouse_DOB", type: "uint256" },
          { name: "_spouse_POB", type: "string" },
          { name: "_spouse_f_name", type: "string" },
          { name: "_spouse_m_name", type: "string" },
          { name: "_spouse_vow", type: "string" },
          { name: "_spouse_sign", type: "string" }
        ],
        name: "addSpouse",
        outputs: [],
        payable: false,
        stateMutability: "nonpayable",
        type: "function"
      },
      {
        constant: true,
        inputs: [{ name: "", type: "uint256" }],
        name: "creators",
        outputs: [
          { name: "id", type: "uint256" },
          { name: "creator_name", type: "string" },
          { name: "creator_DOB", type: "uint256" },
          { name: "creator_POB", type: "string" },
          { name: "creator_f_name", type: "string" },
          { name: "creator_m_name", type: "string" },
          { name: "creator_vow", type: "string" },
          { name: "creator_sign", type: "string" }
        ],
        payable: false,
        stateMutability: "view",
        type: "function"
      },
      {
        inputs: [],
        payable: false,
        stateMutability: "nonpayable",
        type: "constructor"
      }
    ];
    const contractAddress = "0xf939218CeEA8c5F67071F8A072e3d890C4EE6bAA";
    const electionContract = new web3.eth.Contract(
      contractABI,
      contractAddress
    );

      const data=[];
    let committedCert = await electionContract.methods
      .certificates(commited_cert_number)
      .call();
    let committedCreator = await electionContract.methods
      .creators(commited_cert_number)
      .call();
    let committedSpouse = await electionContract.methods
      .spouses(commited_cert_number)
      .call();
      data.push( committedCert,committedCreator,committedSpouse)
      console.log(data)
      return data
  }

  public async getCommited_cert_numberByCreator_Id(id: string) {
    return await this.knex("users")
      .join("certificates", "users.id", "=", "certificates.creator_id")
      .select("users.id", "commited_cert_number")
      .where("users.id", id);
  }

  public async getCommited_cert_numberBySpouse_Id(id: string) {
    return await this.knex("users")
      .join("certificates", "users.id", "=", "certificates.spouse_id")
      .select("users.id", "commited_cert_number")
      .where("users.id", id);
  }
}
