import * as knex from 'knex';

interface Certificate {
    marriage_location_longitude: number;
    marriage_location_latitude: number;
    marriage_date: number;
    witness_name: string;
    creator_vow: string;
    spouse_vow: string;
    e_signature_1: string;
    e_signature_2: string;
    e_signature_witness: string;
    c_full_name: string;
    c_date_of_birth: number;
    c_place_of_birth: string;
    c_parent1_name: string;
    c_parent2_name: string;
    s_full_name: string;
    s_date_of_birth: number;
    s_place_of_birth: string;
    s_parent1_name: string;
    s_parent2_name: string;
    creation_date: number;
    certificate_id: number;
}
export class ProposalCommitService{
    private committed: boolean;
    constructor(private knex: knex){
      this.committed = false;
      // console.log(this.committed)
    }

    public commit = async (certificate: Certificate) =>{
        const Web3 = require('web3');
        const web3 = new Web3("https://ropsten.infura.io/v3/726e035b15274e5bb67aedee4e9f4e1e")
        const privateKey = '06B49DDCFC13CDE20CF428F55DC7E53CE6A8A4A298CC3618FF7F55153C655F1F';
        const account = web3.eth.accounts.privateKeyToAccount('0x' + privateKey);
        web3.eth.accounts.wallet.add(account);
        web3.eth.defaultAccount = account.address;
        console.log(web3.eth.defaultAccount)
        
        const contractABI = [
          {
            constant: true,
            inputs: [{ name: "", type: "uint256" }],
            name: "spouses",
            outputs: [
              { name: "id", type: "uint256" },
              { name: "spouse_name", type: "string" },
              { name: "spouse_DOB", type: "uint256" },
              { name: "spouse_POB", type: "string" },
              { name: "spouse_f_name", type: "string" },
              { name: "spouse_m_name", type: "string" },
              { name: "spouse_vow", type: "string" },
              { name: "spouse_sign", type: "string" }
            ],
            payable: false,
            stateMutability: "view",
            type: "function"
          },
          {
            constant: false,
            inputs: [
              { name: "_creator_name", type: "string" },
              { name: "_creator_DOB", type: "uint256" },
              { name: "_creator_POB", type: "string" },
              { name: "_creator_f_name", type: "string" },
              { name: "_creator_m_name", type: "string" },
              { name: "_creator_vow", type: "string" },
              { name: "_creator_sign", type: "string" }
            ],
            name: "addCreator",
            outputs: [],
            payable: false,
            stateMutability: "nonpayable",
            type: "function"
          },
          {
            constant: true,
            inputs: [],
            name: "certificateCount",
            outputs: [{ name: "", type: "uint256" }],
            payable: false,
            stateMutability: "view",
            type: "function"
          },
          {
            constant: true,
            inputs: [{ name: "", type: "uint256" }],
            name: "certificates",
            outputs: [
              { name: "id", type: "uint256" },
              { name: "marriage_date", type: "uint256" },
              { name: "marriage_location_lat", type: "int256" },
              { name: "marriage_location_lon", type: "int256" },
              { name: "witness_name", type: "string" },
              { name: "witness_sign", type: "string" },
              { name: "creation_date", type: "uint256" }
            ],
            payable: false,
            stateMutability: "view",
            type: "function"
          },
          {
            constant: false,
            inputs: [
              { name: "_marriage_date", type: "uint256" },
              { name: "_marriage_location_lat", type: "int256" },
              { name: "_marriage_location_lon", type: "int256" },
              { name: "_witness_name", type: "string" },
              { name: "_witness_sign", type: "string" },
              { name: "_creation_date", type: "uint256" }
            ],
            name: "addCertificate",
            outputs: [],
            payable: false,
            stateMutability: "nonpayable",
            type: "function"
          },
          {
            constant: false,
            inputs: [
              { name: "_spouse_name", type: "string" },
              { name: "_spouse_DOB", type: "uint256" },
              { name: "_spouse_POB", type: "string" },
              { name: "_spouse_f_name", type: "string" },
              { name: "_spouse_m_name", type: "string" },
              { name: "_spouse_vow", type: "string" },
              { name: "_spouse_sign", type: "string" }
            ],
            name: "addSpouse",
            outputs: [],
            payable: false,
            stateMutability: "nonpayable",
            type: "function"
          },
          {
            constant: true,
            inputs: [{ name: "", type: "uint256" }],
            name: "creators",
            outputs: [
              { name: "id", type: "uint256" },
              { name: "creator_name", type: "string" },
              { name: "creator_DOB", type: "uint256" },
              { name: "creator_POB", type: "string" },
              { name: "creator_f_name", type: "string" },
              { name: "creator_m_name", type: "string" },
              { name: "creator_vow", type: "string" },
              { name: "creator_sign", type: "string" }
            ],
            payable: false,
            stateMutability: "view",
            type: "function"
          },
          {
            inputs: [],
            payable: false,
            stateMutability: "nonpayable",
            type: "constructor"
          }
        ];
        const contractAddress = "0xf939218CeEA8c5F67071F8A072e3d890C4EE6bAA";
        const electionContract = new web3.eth.Contract(contractABI, contractAddress);
        (async()=>{
            try{
                let hashInfo = await electionContract.methods.addCertificate(certificate.marriage_date,
                    certificate.marriage_location_latitude,
                    certificate.marriage_location_longitude,
                    certificate.witness_name,
                    certificate.e_signature_witness,
                    certificate.creation_date).send({from:web3.eth.defaultAccount, gas: 8000000})
                await electionContract.methods.addCreator(certificate.c_full_name,
                    certificate.c_date_of_birth,
                    certificate.c_place_of_birth,
                    certificate.c_parent1_name,
                    certificate.c_parent2_name,
                    certificate.creator_vow,
                    certificate.e_signature_1).send({from:web3.eth.defaultAccount, gas: 8000000},(err:Error,TxnHash:string)=>{
                        console.log(err, TxnHash)
                    })

                await electionContract.methods.addSpouse(certificate.s_full_name,
                    certificate.s_date_of_birth,
                    certificate.s_place_of_birth,
                    certificate.s_parent1_name,
                    certificate.s_parent2_name,
                    certificate.spouse_vow,
                    certificate.e_signature_2).send({from:web3.eth.defaultAccount, gas: 8000000},(err:Error,TxnHash:string)=>{
                        console.log(err, TxnHash)
                    })
                let certCount = await electionContract.methods.certificateCount().call();
                let committedCert = await electionContract.methods.certificates(certCount).call()
                let committedCreator = await electionContract.methods.creators(certCount).call()
                let committedSpouse = await electionContract.methods.spouses(certCount).call()

                console.log(certCount)
                console.log('txnHash',hashInfo.transactionHash)
                console.log(committedCert)
                console.log(committedCreator)
                console.log(committedSpouse)
                let id = await this.saveHash(hashInfo.transactionHash, certCount, certificate.certificate_id);
                this.committed = true;
                return id;
            }catch(e){
                console.log(e);
                return e;
            }
        })();
    }


    public saveHash = async(txnHash: string, certNum: number, certificateId: number) =>{
      let id = await this.knex('certificates')
                      .update({
                        TX: txnHash,
                        commited_cert_number: certNum
                      })
                      .where('id', certificateId)
                      .returning('id');
      console.log('saveHash', id);
      return id;
    }

    public checkCommitted = ()=>{
      console.log('thiscommitted', this.committed);

        return this.committed;
    }
  }
