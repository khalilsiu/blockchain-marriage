
import { hashPassword } from "../hash";
import * as Knex from 'knex';

export class LoginService {

  constructor(private knex:Knex){

  }

  getUsers(){
    return this.knex.select("*").from('users');
  }

  async createUser(userName:string,password:string,firstname:string,middleName:string,lastName:string,email:string){
       let hashedPassword= await hashPassword(password)
       const fullName =firstname+''+middleName+''+lastName;
       console.log('Knex Start')
    return this.knex.table("users").insert({
        username:userName,password:hashedPassword,first_name:firstname,middle_name:middleName,last_name:lastName,email:email,full_name:fullName
    });
  }

}