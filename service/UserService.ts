import * as Knex from "knex";
import { hashPassword } from "../hash";
export class UserService {
  constructor(private knex: Knex) {}

  public getUsers() {
    return this.knex.select("*").from("users");
  }

  public async getCertInfo(id:string){
    return await this.knex
    .select()
    .from("certificates")
    .where("creator_id", id)
  }

  public async getUUID(id:string){
    return await this.knex
    .select()
    .from("proposal_keys")
    .where("creator_id", id)
  }

  public async getUUID2(id:string){
    return await this.knex
    .select()
    .from("proposal_keys")
    .where("spouse_id", id)
  }

  public async insertPaymentInfo(
    id: number,
    receipt_url: string,
    charged: string
  ) {
    let certId: number = await this.knex
      .select()
      .from("certificates")
      .where("creator_id", id)
      .update({ receipt_url: receipt_url, charged: charged })
      .returning('id');
    console.log('CERTID',certId);
    return certId
  }

  public async insertUUID(id:number, certId: number){
    const uuidv4 = require('uuid/v4');
    const UUID=uuidv4();
    console.log("UUID",UUID)
    let spouseId = (await this.knex
    .select('spouse_id')
    .from('certificates')
    .innerJoin('users','creator_id','users.id')
    .where('creator_id',id))[0]['spouse_id']
    console.log('spouseId', spouseId)
    let proposalId: number = await this.knex
      .insert({
        UUID:UUID,
        spouse_id: spouseId,
        creator_id: id,
        certificate_id: certId
      })
      .into('proposal_keys')
      .returning('id');
      console.log(proposalId)

  }

  public async sendUUID(id:number){
    return await this.knex
      .select()
      .from("proposal_keys")
      .where("creator_id", id)
  }

  public createSpouse = async (
    spouseFName: string,
    spouseMName: string,
    spouseLName: string,
    spouseDOB: Date,
    spousePOB: Date,
    spouseParent1Name: string,
    spouseParent2Name: string
  ) => {
    let id = await this.knex
      .insert({
        first_name: spouseFName,
        middle_name: spouseMName,
        last_name: spouseLName,
        date_of_birth: spouseDOB,
        place_of_birth: spousePOB,
        parent1_name: spouseParent1Name,
        parent2_name: spouseParent2Name
      })
      .into("users")
      .returning("id");
    return id;
  };

  public updateSpouse = async (
    userId: number,
    spouseFName: string,
    spouseMName: string,
    spouseLName: string,
    spouseDOB: Date,
    spousePOB: Date,
    spouseParent1Name: string,
    spouseParent2Name: string
  ) => {
    let spouseId = (await this.knex.select('spouse_id').from('certificates').innerJoin('users','spouse_id','users.id').where('creator_id',userId))[0]['spouse_id'];
    let id = await this.knex('users')
      .update({
        first_name: spouseFName,
        middle_name: spouseMName,
        last_name: spouseLName,
        date_of_birth: spouseDOB,
        place_of_birth: spousePOB,
        parent1_name: spouseParent1Name,
        parent2_name: spouseParent2Name
      })
      .where('id',spouseId)
      .returning("id");
    return id;
  };

  public insertCreatorInfo = async (
    userId: number,
    creatorFName: string,
    creatorMName: string,
    creatorLName: string,
    creatorDOB: Date,
    creatorPOB: Date,
    creatorParent1Name: string,
    creatorParent2Name: string
  ) => {
    let id = await this.knex("users")
      .update({
        first_name: creatorFName,
        middle_name: creatorMName,
        last_name: creatorLName,
        date_of_birth: creatorDOB,
        place_of_birth: creatorPOB,
        parent1_name: creatorParent1Name,
        parent2_name: creatorParent2Name
      })
      .where("id", userId)
      .returning("id");
    return id;
  };

  public sendConfirmationEmail(email:string,firstname:string,TX:any,receipt_url:any) {
    const mailgun = require("mailgun-js");
    const DOMAIN = "app.pontdesartsweb.info";
    const mg = mailgun({
      apiKey: "065fbc56b8b48c92f3a0225fed542d2b-c50f4a19-334776c3",
      domain: DOMAIN
    });
    const data = {
      from:
        "XXXXXX <postmaster@app.pontdesartsweb.info>",
      to: `${email}`,subject: "Payment confirmation",text: `Hello ${ firstname }, this is a confirmation that we just received your online payment. Please see ${receipt_url} for your reference!Your transaction code is ${TX}, at the predetermined location and here is a copy of your detailed invoice. Don't hesitate to contact us for any questions or concerns. Thank you!`
    };
    mg.messages().send(data, function(error:any, body:any) {
      console.log(body);
    });
  }

  public sendInvitations(email:string,UUID:string){
    const mailgun = require("mailgun-js");
    const DOMAIN = "app.pontdesartsweb.info";
    const mg = mailgun({
      apiKey: "065fbc56b8b48c92f3a0225fed542d2b-c50f4a19-334776c3",
      domain: DOMAIN
    });
    const data = {
      from:
        "XXXXXX <postmaster@sandboxc7afd4dd4d9646fba2c597755b726138.mailgun.org>",
      to: `${email}`,subject: "Marriage confirmation",text: `Hello ,
      http://deploy.pontdesartsweb.info//committed/?ID=${UUID}`
    };
    mg.messages().send(data, function(error:any, body:any) {
      console.log(body);
    });
  }

  public async changePassword(
    id: number,
    newPassword: string
  ) {
    let newHashedPassword = await hashPassword(newPassword)
    console.log(id)
    console.log(newPassword)
   const done= await this.knex
      .select()
      .from("users")
      .where("id", id)
      .update({'password': newHashedPassword});
      console.log(done)
  }

  public async searchByUserName(fullName:string){
    return await this.knex
    .select()
    .from("users")
    .where("full_name", fullName)
  }

  public async searchByTX(tx:string){
    return await this.knex
    .select()
    .from("certificates")
    .where("TX",tx)
  }

  public async searchByID(id:string){
    return await this.knex('users')
    .join('certificates', 'users.id', '=', 'certificates.creator_id')
    .select('users.id', 'certificates.TX', 'certificates.marriage_date','users.full_name','certificates.location ','certificates.e_signature_witness','certificates.e_signature_1','certificates.e_signature_2').where('certificates.creator_id',id)
  }

  public async searchByID2(id:string){
    return await this.knex('users')
    .join('certificates', 'users.id', '=', 'certificates.spouse_id')
    .select('users.id', 'certificates.TX','users.full_name','certificates.spouse_id').where('certificates.spouse_id',id)
  }

  public async getTXByCreator_Id(id:string){
    return await this.knex('users')
    .join('certificates', 'users.id', '=', 'certificates.creator_id')
    .select('users.id','certificates.TX','certificates.creator_id','certificates.spouse_id').where('users.id',id)
  }

  public async getTXBySpouse_Id(id:string){
    return await this.knex('users')
    .join('certificates', 'users.id', '=', 'certificates.spouse_id')
    .select('users.id','certificates.TX','certificates.creator_id','certificates.spouse_id').where('users.id',id)
  }

}


