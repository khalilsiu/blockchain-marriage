import * as knex from "knex";

export class ProposalLinkService {
  constructor(private knex: knex) {
    // console.log(this.knex);
  }

  public getCert = async (proposalKey: string) => {
    let certificateId;
    let certificateInfo = await this.knex
      .select("*")
      .from("proposal_keys")
      .where("UUID", proposalKey);

    // console.log(certificateInfo)

    if (certificateInfo[0]) {
      certificateId = certificateInfo[0]["certificate_id"];
      let certificate = await this.knex
        .select(
          "certificates.*",
          "u.first_name as c_first_name",
          "u.middle_name as c_middle_name",
          "u.last_name as c_last_name",
          "u.date_of_birth as c_date_of_birth",
          "u.place_of_birth as c_place_of_birth",
          "u.parent1_name as c_parent1_name",
          "u.parent2_name as c_parent2_name",
          "u.email as c_email",
          "s.first_name as s_first_name",
          "s.middle_name as s_middle_name",
          "s.last_name as s_last_name",
          "s.date_of_birth as s_date_of_birth",
          "s.place_of_birth as s_place_of_birth",
          "s.parent1_name as s_parent1_name",
          "s.parent2_name as s_parent2_name",
          "s.email as s_email",
          "stories.*",
          "stories.id as stories_id",
          "photos.*",
          "photos.id as photos_id"
        )
        .from("certificates")
        .innerJoin("users AS s", "spouse_id", "s.id")
        .innerJoin("users AS u", "creator_id", "u.id")
        .innerJoin("stories", "certificates.id", "certificate_id")
        .innerJoin("photos", "stories.id", "story_id")
        .where("certificates.id", certificateId)
        .orderBy("photos.order");

        // console.log(certificate)

      return certificate;
    } else {
        return [];
    }
  };
}
