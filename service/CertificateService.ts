import * as knex from "knex";

export class CertificateService {
  constructor(private knex: knex) {
  }
  public createCert = async (
    userId: number,
    spouseId: string,
    marriageLocationLat: number,
    marriageLocationLng: number,
    marriageDate: Date,
    marriageLocation: string,
    witnessName: string,
    creatorVow: string,
    spouseVow: string,
    creatorSign: string,
    spouseSign: string,
    witnessSign: string
  ) => {

    let id = await this.knex
      .insert({
        creator_id: userId,
        spouse_id: parseInt(spouseId),
        marriage_location_latitude: marriageLocationLat,
        marriage_location_longitude: marriageLocationLng,
        marriage_date: marriageDate,
        location: marriageLocation,
        witness_name: witnessName,
        creator_vow: creatorVow,
        spouse_vow: spouseVow,
        e_signature_1: creatorSign,
        e_signature_2: spouseSign,
        e_signature_witness: witnessSign
      })
      .into("certificates")
      .returning("id");


    return id;
  };

  public updateCert = async (
    certId: number,
    marriageLocationLat: number,
    marriageLocationLng: number,
    marriageDate: Date,
    marriageLocation: string,
    witnessName: string,
    creatorVow: string,
    spouseVow: string,
    creatorSign: string,
    spouseSign: string,
    witnessSign: string
  ) => {

    let id = await this.knex('certificates')
      .update({
        marriage_location_latitude: marriageLocationLat,
        marriage_location_longitude: marriageLocationLng,
        marriage_date: marriageDate,
        location: marriageLocation,
        witness_name: witnessName,
        creator_vow: creatorVow,
        spouse_vow: spouseVow,
        e_signature_1: creatorSign,
        e_signature_2: spouseSign,
        e_signature_witness: witnessSign
      })
      .where('id', certId)
      .returning("id");


    return id;
    }

  public getCert = async (userId: number) => {
    let certificate;
    let photoExists = await this.knex.select('stories.*')
    .from('certificates')
      .innerJoin('users AS s', 'spouse_id', 's.id')
      .innerJoin('users AS u', 'creator_id', 'u.id')
      .innerJoin('stories', 'certificates.id','certificate_id')
      .innerJoin('photos','stories.id','story_id')
      .where('u.id',userId)
    // console.log('photoExists',photoExists)
    if (!photoExists[0]){
      certificate = await this.knex.select('certificates.*','u.first_name as c_first_name','u.middle_name as c_middle_name','u.last_name as c_last_name','u.date_of_birth as c_date_of_birth','u.place_of_birth as c_place_of_birth','u.parent1_name as c_parent1_name', 'u.parent2_name as c_parent2_name', 'u.email as c_email','s.first_name as s_first_name','s.middle_name as s_middle_name','s.last_name as s_last_name','s.date_of_birth as s_date_of_birth','s.place_of_birth as s_place_of_birth','s.parent1_name as s_parent1_name', 's.parent2_name as s_parent2_name', 's.email as s_email','stories.*','stories.id as stories_id')
      .from('certificates')
      .innerJoin('users AS s', 'spouse_id', 's.id')
      .innerJoin('users AS u', 'creator_id', 'u.id')
      .innerJoin('stories', 'certificates.id','certificate_id')
      .where('u.id', userId)
      // console.log(certificate)
      return certificate
    }
    else{
      certificate = await this.knex.select('certificates.*','u.first_name as c_first_name','u.middle_name as c_middle_name','u.last_name as c_last_name','u.date_of_birth as c_date_of_birth','u.place_of_birth as c_place_of_birth','u.parent1_name as c_parent1_name', 'u.parent2_name as c_parent2_name', 'u.email as c_email','s.first_name as s_first_name','s.middle_name as s_middle_name','s.last_name as s_last_name','s.date_of_birth as s_date_of_birth','s.place_of_birth as s_place_of_birth','s.parent1_name as s_parent1_name', 's.parent2_name as s_parent2_name', 's.email as s_email','stories.*','stories.id as stories_id','photos.*','photos.id as photos_id')
      .from('certificates')
      .innerJoin('users AS s', 'spouse_id', 's.id')
      .innerJoin('users AS u', 'creator_id', 'u.id')
      .innerJoin('stories', 'certificates.id','certificate_id')
      .innerJoin('photos','stories.id','story_id')
      .where('u.id',userId)
      .orderBy('photos.order')
      // console.log(certificate)
      return certificate
    }
  }

  public checkPaid = async (certId: number)=>{
    let paid = (await this.knex
                      .select('charged')
                      .from('certificates')
                      .where('id', certId))[0].charged == 'succeeded';
    console.log('pdddd',paid)
    return paid
  }
}
