import * as Knex from "knex";
const puppeteer = require('puppeteer');


export class GenPDFService {

  constructor(private knex: Knex) {}


  public async genPDF(ID:string){
   
    var options = {
        format: 'A5',
        printBackground: true,
        path: `./public/PDF/${ID}_Certificate.pdf`
    }

    const browser = await puppeteer.launch({
        args: ['--no-sandbox'],
        headless: true
    });
    const page = await browser.newPage();
    await page.goto(`http://localhost:8081/public/cert/?ID=${ID}`, {
        waitUntil: 'networkidle0'
    });
    await page.pdf(options);
    await browser.close();
    await this.knex
      .select()
      .from("users")
      .where("id", ID)
      .update({'PDF_path': `./public/PDF/${ID}_Certificate.pdf`});
  }
}