import * as knex from 'knex';



export class StoryService{
    constructor(private knex: knex){
    }

    public storySave = async (certId: string, description: string, location: string, placeId: string, locationLat: number, locationLng: number) =>{
        let id: string = await this.knex.insert({
            certificate_id: parseInt(certId),
            description: description,
            location: location,
            place_id: placeId,
            latitude: locationLat,
            longitude: locationLng
        }).into('stories').returning('id')
        // console.log('storySavee', id)
        return id;
    }

    public updateStory = async (storyId: number, certId: string, description: string, location: string, placeId: string, locationLat: number, locationLng: number) =>{
        let id: string = await this.knex('stories')
        .update({
            certificate_id: parseInt(certId),
            description: description,
            location: location,
            place_id: placeId,
            latitude: locationLat,
            longitude: locationLng
        })
        .where('id', storyId)
        .returning('id')
        // console.log('updateStoryy', id)
        return id;
    }

    public photoSave = async(storyId: string, filePath: string, index: number) =>{
        let id = await this.knex.insert({
            story_id: parseInt(storyId),
            photo: filePath,
            order: index
        }).into('photos').returning('id');
        return id
    }

    public updatePhoto = async(photoId:number, order:number)=>{
        // console.log('updatePhoto',photoId)
        let id = await this.knex('photos')
        .update({
            order: order
        })
        .where('id',photoId)
        .returning('id')
        return id;
    }

    public removePhoto = async (originalId: number) =>{
        let id = await this.knex
        .delete('*')
        .from('photos')
        .where('id',originalId)
        .returning('id');
        return id
    }

}





