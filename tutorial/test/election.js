// test is important since deploying costs ether
var assert = require("assert");
var Election = artifacts.require("./Election.sol");

// using mocha and chai to run test cases
contract("Election", function(accounts) {
  // since we want to pass the promise as a promise chain
  // to be used to pass another variable
  let electionInstance;
  it("initializes with two candidates", function() {
    return Election.deployed()
      .then(function(instance) {
        return instance.candidatesCount();
        // the returning value is a promise, needs to use .then
      })
      .then(function(count) {
        assert.equal(count, 2);
      });
  });

  it("initializes with the correct name", function() {
    return Election.deployed()
      .then(function(instance) {
        electionInstance = instance;
        return electionInstance.candidates(1);
      })
      .then(function(candidate) {
        assert.equal(candidate[0], 1, "contains the correct id");
        assert.equal(candidate[1], "Candidate 1", "contains the correct name");
        assert.equal(candidate[2], 0, "contains the correct votes count");
        return electionInstance.candidates(2);
      })
      .then(function(candidate) {
        assert.equal(candidate[0], 2, "contains the correct id");
        assert.equal(candidate[1], "Candidate 2", "contains the correct name");
        assert.equal(candidate[2], 0, "contains the correct votes count");
      });
  });

  it("increments the vote count when voting", () => {
    return Election.deployed()
      .then(instance => {
        electionInstance = instance;
        candidateId = 1;
        return electionInstance.vote(candidateId, { from: accounts[0] });
      })
      .then(receipt => {
          // ensure it has 1 log, the event is votedEvent, make sure that the argument of the vote function equals to the one above
            assert.equal(receipt.logs.length,1,'an event was triggered')
          assert.equal(receipt.logs[0].event, "votedEvent", "the event type is correct")
          assert.equal(receipt.logs[0].args._candidateId.toNumber(), candidateId, "candidate id is correct")
        // this is how you get access to the mapped value >> true
        return electionInstance.voters(accounts[0]);
      })
      .then(voted => {
        assert(voted, "the voter was marked as voted");
        return electionInstance.candidates(candidateId);
      })
      .then(candidate => {
        let voteCount = candidate[2];
        assert.equal(voteCount, 1, "increments the candidate's vote count");
      });
  });

  it("throws an exception for invalid candidates", async () => {
    return Election.deployed()
    .then(instance => {
      electionInstance = instance;
      return electionInstance.vote(99, {from: accounts[1]})
    }).then(assert.fail).catch(error => {
      // check if it has revert! that specifies that the txn didnt happen
      assert(
        error.message.indexOf("revert") >= 0,
        "error message must contain revert"
      );
      return electionInstance.candidates(1);
    })
    .then(candidate1 => {
      let voteCount = candidate1[2];
      assert.equal(voteCount, 1, "candidate 1 did not receive any votes");
      return electionInstance.candidates(2);
    })
    .then(candidate2 => {
      let voteCount = candidate2[2];
      assert.equal(voteCount, 0, "candidate 2 did not receive any voted");
    });
  })

  it("throws an exception for double voting", () => {
    return Election.deployed()
      .then(instance => {
        electionInstance = instance;
        candidateId = 2;
        electionInstance.vote(candidateId, { from: accounts[1] });
        return electionInstance.candidates(candidateId);
      })
      .then(candidate => {
          let voteCount = candidate[2];
          assert.equal(voteCount, 1, "accepts first vote")
          return electionInstance.vote(candidateId, {from: accounts[1]});
      }).then(assert.fail).catch((error)=>{
          assert(error.message.indexOf('revert') >= 0, "error contains revert")
          return electionInstance.candidates(1)
      }).then(candidate1 => {
        let voteCount = candidate1[2];
        assert.equal(voteCount, 1, "candidate 1 did not receive any votes");
        return electionInstance.candidates(2);
      })
      .then(candidate2 => {
        let voteCount = candidate2[2];
        assert.equal(voteCount, 1, "candidate 2 did not receive any voted");
      });
  });
});
