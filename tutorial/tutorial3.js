var Tx = require('ethereumjs-tx').Transaction;
const Web3 = require('web3')
// test network, remote node. but we do not want it to know our sensitive data
const web3 = new Web3('https://ropsten.infura.io/v3/abb846960276472194c1db0fb81d9baa')

const account1 = '0xD27e73E2C878A7edEC905a2202d23465db3FEEb2'
const account2 = '0x010Ce083CC97E2820cA0ba8c61d950c65c108a2f'

// export PRIVATE_KEY_1='10d433839157aa6c6d0bb41f87376b4810759a289833843ea6d00a0e2711938c' in command line

const privateKey1 = Buffer.from(process.env.PRIVATE_KEY_1, 'hex');
const privateKey2 = Buffer.from(process.env.PRIVATE_KEY_2, 'hex');

web3.eth.getTransactionCount(account1,(err, txCount)=>{
    // build the transaction
    // gas is the reward to the miner
    // nonce: safeguard against double spend, this value will increment per tx
    // console.log(txCount)
    const txObject = {
        nonce: web3.utils.toHex(txCount),
        to: account2,
        value: web3.utils.toHex(web3.utils.toWei('1','ether')),
        gasLimit: web3.utils.toHex(21000),
        gasPrice: web3.utils.toHex(web3.utils.toWei('10','gwei'))
    }

    // console.log(txObject)

    // sign the transaction
    // version issue
    const tx = new Tx(txObject, {'chain':'ropsten'})
    tx.sign(privateKey1)

    //serialize and convert into hex string to be passed as raw
    const serializedTransaction = tx.serialize()
    const raw = '0x' + serializedTransaction.toString('hex')

    // broadcast the transaction
    web3.eth.sendSignedTransaction(raw, (err,txHash)=> {
        console.log(err);
        console.log('txHash:', txHash)
    })
})

// need to convert them to binary as buffer in order to use to sign

