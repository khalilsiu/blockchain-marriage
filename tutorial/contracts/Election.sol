pragma solidity ^0.5.0;

contract Election {
    // Model a Candidate - describe it, name, votecount, id
    struct Candidate {
        uint id;
        string name;
        uint voteCount;
    }
    // Store accounts that have voted
    mapping(address=>bool) public voters;

    // Store Candidate
    // Fetch Candidate
    mapping(uint => Candidate) public candidates;
    // Store Candidate Count

    uint public candidatesCount;

    //voted event, we want this event to be triggered everytime the vote function is called
    event votedEvent (
        uint indexed _candidateId
    );

    constructor () public {
        addCandidate("Candidate 1");
        addCandidate("Candidate 2");
    }

    function addCandidate(string memory _name) private {
        candidatesCount ++;
        candidates[candidatesCount] = Candidate(candidatesCount, _name, 0);
    }

    // solidity allows us to pass metadata to a function
    function vote(uint _candidateId) public{
        // check if the address hasnt been voted before
        // if condition is true, execute, if not, stop execution
        // required that the incoming address does not exist in the voters mapping
        require(!voters[msg.sender],'voter has voted');

        // require a valid candidate
        require(_candidateId > 0 && _candidateId <= candidatesCount, "candidate does not exist");
        // if the statements are false, the codes below wont be run, and GAS would be refunded.
        // however, the ones above would be credited. NEED TO TEST

        // record that voter has voted
        // msg is a part of the metadata that get passed in, sender is the account from which the function is sent
        // know who is voting, but to know if they voted >> mapping
        voters[msg.sender] = true;
        //update candidate vote count
        // this is how you get and change the candidate's vote count by id
        candidates[_candidateId].voteCount++;

        //trigger voted event
        emit votedEvent(_candidateId);
    }

}

// truffle console
// vote using an account
// web3.eth.getAccounts().then(x=>arr=x)
// app.vote(1, {from: arr[0]})

// txn receipt
// receipt:
//    { transactionHash:
//       '0x0414399b8bbed964098b7bdb4272176331ed93f9362b3bf44632c91e6fbbb48f',
//      transactionIndex: 0,
//      blockHash:
//       '0x01fa192524dc45978129c5f37316a657a7a4c36e51f2f31574dfe2f918f0e6a7',
//      blockNumber: 13,
//      from: '0x0e19bae250a9218ecf2e61995e9f2b7d263a6a20',
//      to: '0x036c4790bc66a17047155c21d4022f643bda52e0',
//      gasUsed: 62347,
//      cumulativeGasUsed: 62347,
//      contractAddress: null,
//      logs: [],
//      status: true,
//      logsBloom:
//       '0x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',
//      v: '0x1c',
//      r:
//       '0xbb68d1ba78ce4d0c9c9ee631cd253dfd429ae08afba79eb1d53002559365bfbd',
//      s:
//       '0x0e3cf789f918d96c137f29074eb79b47d0befb9f8b7453c3a97365682506fa',
//      rawLogs: [] },
//   logs: [] }

// fault cases have to be tested