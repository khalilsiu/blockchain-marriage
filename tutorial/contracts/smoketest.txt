pragma solidity ^0.5.0;

// create smoke test to ensure that everything set up correctly, and respond correctly
contract Election {
    // Store candidate
    // Read candidate
    string public candidate;
    //declaring the state variable such that we can read and write it. When it is declared public, state variable will be assigned a GETTER function, without the need to write one to read the value ourselves
    // Constructor: new version
    // function same name as contract, as function is run whenever we deploy to the blockchain, therefore has to be public
     constructor () public {
        // without _, the variable is a state variable it is accessible within the contract and belongs to the entire contract
        candidate = "Candidate 1"; 
    }
}