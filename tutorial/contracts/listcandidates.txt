pragma solidity ^0.5.0;

contract Election {
    // code cannot be changed on BC, migrates are run ONCE, but when you need to for development, add truffle migrate --reset, if not, you will be given a new address
    // Model a Candidate - describe it, name, votecount, id
    // can create a struct, you need to create a variable
    struct Candidate {
        uint id;
        string name;
        uint voteCount;
    }
    // Store Candidate
    // Fetch Candidate
    // Mapping in solidity allows us to do a hash to store key and value pairs >> declaring candidate public, we can get a getter function
    // We are changing the state of the contract and writing on the BC
    mapping(uint => Candidate) public candidates;
    mapping(address => bool) public voters;
    // Store Candidate Count
    // in solidity, we have no way to determine the size or loop through the mapping, any key will return a default value: if value doesnt exist, it will be a blank candidate >> cant determine size
    // therefore we need a counter, increment each time, we can also iterate by the number
    uint public candidatesCount;
    event votedEvent (
        uint indexed _candidateId
    );

    constructor () public {
        addCandidate("Candidate 1");
        addCandidate("Candidate 2");
    }

    // local variable needs _, not state variable
    // private: do not want public to run this function
    function addCandidate(string memory _name) private {
        candidatesCount ++;
        candidates[candidatesCount] = Candidate(candidatesCount, _name, 0);
    }

    function vote (uint _candidateId) public {
        // require that they haven't voted before
        require(!voters[msg.sender],'sender not authorized');

        // require a valid candidate
        require(_candidateId > 0 && _candidateId <= candidatesCount,'sender not authorized');

        // record that voter has voted
        voters[msg.sender] = true;

        // update candidate vote Count
        candidates[_candidateId].voteCount ++;

        // trigger voted event
        emit votedEvent(_candidateId);
    }

    // in truffle console
    // app.candidates(1); >> 
    // Result {
    //   '0': <BN: 1>,
    //   '1': 'Candidate 1',
    //   '2': <BN: 0>,
    //   id: <BN: 1>,
    //   name: 'Candidate 1',
    //   voteCount: <BN: 0> }
    // there is also result for 99
    // app.candidates(1).then(function(c){candidate=c}) >> assigns to a variable
    // can use candidate.id to get the id, also, candidate.id.toNumber() to number

    // Voters
    // all accounts connected can vote. in truffle console: web3.eth.getAccounts()
    // web3.eth.getAccounts().then(x=>arr=x) > assign to variable so that arr[0] 
}
