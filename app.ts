import * as express from "express";
import * as bodyParser from "body-parser";
import * as passport from "passport";
import * as expressSession from "express-session";
import * as path from "path";
import * as Knex from 'knex'
import { UserService } from "./service/UserService"
import{UserRouter} from "./router/UserRouter"
import { loginFlow } from "./guard";
import * as multer from 'multer'
import { ProposalLinkService } from "./service/ProposalLinkService";
import { ProposalLinkRouter } from "./router/ProposalLinkRouter";
import {GetInfoFromBlockChainService} from'./service/GetInfoFromBlockChainService'
import {GetInfoFromBlockChainRouter} from'./router/GetInfoFromBlockChainRouter'

const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])

const app = express();

app.use(express.static(path.join(__dirname)))
app.use(express.static(path.join(__dirname,'/public')))

app.use(
  expressSession({
    secret: "fuckChina",
    resave: true,
    saveUninitialized: true
  })
);

app.use(passport.initialize());
app.use(passport.session());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


import { isLoggedIn } from "./guard";
import "./passport";
import { LoginService } from "./service/LoginService";
import { LoginRouter } from "./router/LoginRouter";
import { PublicRouter } from "./router/PublicRouter";
import { CertificateRouter } from "./router/CertificateRouter";
import { StoryRouter} from './router/StoryRouter'
import {CertificateService} from './service/CertificateService'
import {StoryService} from './service/StoryService';
import {PublicService} from './service/PublicService';
import {ProposalLinkStoryRouter} from './router/ProposalLinkStoryRouter'
import {ProposalLinkUserRouter} from './router/ProposalLinkUserRouter'
import { ProposalCommitService } from "./service/ProposalCommitService";
import { ProposalCommitRouter } from "./router/ProposalCommitRouter";

import {GenPDFService} from './service/GenPDFService';
import {GenPDFRouter} from "./router/GenPDFRouter";
import { CommittedRouter } from "./router/CommittedRouter";


const loginService = new LoginService(knex);
const loginRouter = new LoginRouter(loginService);

const publicService = new PublicService(knex);
const publicRouter= new PublicRouter(publicService);
app.use("/public",publicRouter.router());

app.use("/reg", loginRouter.router());

const userService = new UserService(knex)
const userRouter = new UserRouter(userService)

app.use("/user",isLoggedIn, userRouter.router());


app.get('/auth/google',passport.authenticate('google',{
  scope:['email','profile']}));

app.get('/auth/google/callback',
  (...rest)=>
  passport.authenticate('google',loginFlow(...rest))(...rest))

app.post(
  "/login",
  passport.authenticate("local", { failureRedirect: "/login" }),
  (req, res) => {
    res.redirect(`/`);
  }
);


let i = 0;
const storage = multer.diskStorage({
  destination: (req, file, cb) =>{
    cb(null, `${__dirname}/uploads`)
    
  },
  filename: (req,file,cb) =>{
    cb(null, `${req.user.id}-${file.fieldname}-${Date.now()}-${i++}.${file.mimetype.split('/')[1]}`)
  }
})
console.log(`${__dirname}/uploads`)

const upload = multer({storage})
const storyService = new StoryService(knex);
const certificateService = new CertificateService(knex);
const proposalCommitService = new ProposalCommitService(knex);

const storyRouter = new StoryRouter(upload, storyService)
const certificateRouter = new CertificateRouter(certificateService);
const proposalLinkStoryRouter = new ProposalLinkStoryRouter(upload,storyService);
const proposalLinkUserRouter = new ProposalLinkUserRouter(userService)

const genPDFService=new GenPDFService(knex);
const genPDFRouter = new GenPDFRouter(genPDFService)
const proposalLinkService = new ProposalLinkService(knex);
const proposalCommitRouter = new ProposalCommitRouter(proposalCommitService, proposalLinkService)
const proposalLinkRouter = new ProposalLinkRouter(proposalLinkService, certificateService)
const committedRouter = new CommittedRouter(proposalLinkService);

const getInfoFromBlockChainService= new GetInfoFromBlockChainService(knex);
const getInfoFromBlockChainRouter = new GetInfoFromBlockChainRouter(getInfoFromBlockChainService);


app.use('/proposal-cert',proposalLinkRouter.router());
app.use('/proposal-story',proposalLinkStoryRouter.router())
app.use('/proposal-user',proposalLinkUserRouter.router())
app.use('/proposal-commit',proposalCommitRouter.router())
app.use('/committed',committedRouter.router());
app.use('/proposal', (req,res)=>{
  res.sendFile(path.join(__dirname,'public/proposal/proposal.html'))
})
app.use('/committed',(req,res)=>{
  console.log('committed is run')
  res.sendFile(path.join(__dirname,'public/committed/committed.html'))
})

app.use(isLoggedIn,express.static(path.join(__dirname, "frontend/index")));
app.use("/", certificateRouter.router());
app.use('/uploads', storyRouter.router());
app.use("/pdf", genPDFRouter.router());
app.use("/getData",getInfoFromBlockChainRouter.router())


  //app.post('/oauth/facebook',passport.authenticate('facebookToken', { session: false }), UsersController.facebookOAuth);


app.get('/logout',(req,res)=>{
  
  req.logOut();
  res.redirect('/')
})


const PORT = 8081;

app.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}/`);
});
