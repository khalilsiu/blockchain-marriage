import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    return knex.schema.alterTable('photos',(table)=>{
        table.integer('story_id').unsigned();
        table.foreign('story_id').references('stories.id')

    })
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.alterTable('photos',(table)=>{
        table.dropColumn('story_id')
    })
}

