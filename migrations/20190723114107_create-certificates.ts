import * as Knex from "knex";


export async function up(knex: Knex) {
    const hasTable = await knex.schema.hasTable("certificates");
    if(!hasTable){
        return knex.schema.createTable("certificates",(table)=>{
            table.increments();
            table.float('marriage_location_longitude')
            table.float('marriage_location_latitude')
            table.date('marriage_date')
            table.string('witness_name')
            table.text('creator_vow')
            table.text('spouse_vow')
            table.text('e_signature_1')
            table.text('e_signature_2')
            table.text('e_signature_witness')
            table.integer("creator_id").unsigned();
            table.integer("spouse_id").unsigned();
            table.foreign('creator_id').references('users.id')
            table.foreign('spouse_id').references('users.id')
            table.timestamps(false,true);
        });
    }else{
        return Promise.resolve();
    }
}


export async function down(knex: Knex) {
    return knex.schema.dropTableIfExists("certificates");
}

