import * as Knex from "knex";


export async function up(knex: Knex) {
    const hasTable = await knex.schema.hasTable("locations");
    if(!hasTable){
        return knex.schema.createTable("locations",(table)=>{
            table.increments();
            table.float('location_longitude')
            table.float('location_latitude')
        });
    }else{
        return Promise.resolve();
    }
}


export async function down(knex: Knex) {
    return knex.schema.dropTableIfExists("locations");
}

