import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    return knex.schema.alterTable('certificates',(table)=>{
        table.string('charged').alter()
    })
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.alterTable('certificates',(table)=>{
        table.boolean("charged").alter()
    })
}


