import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    return knex.schema.alterTable('certificates',(table)=>{
        table.string('location')
    })
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.alterTable('certificates',(table)=>{
        table.dropColumn('location')
    })
}

