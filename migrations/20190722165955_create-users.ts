import * as Knex from "knex";


export async function up(knex: Knex) {
    const hasTable = await knex.schema.hasTable("users");
    if(!hasTable){
        return knex.schema.createTable("users",(table)=>{
            table.increments();
            table.string("username");
            table.string("password");
            table.string("name");
            table.string("surname");
            table.date("date_of_birth");
            table.string("place_of_birth");
            table.string("parent1_name")
            table.string("parent2_name")
            table.timestamps(false,true);
        });
    }else{
        return Promise.resolve();
    }
}


export async function down(knex: Knex) {
    return knex.schema.dropTableIfExists("users");
}

