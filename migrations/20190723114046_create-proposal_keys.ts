import * as Knex from "knex";


export async function up(knex: Knex){
    const hasTable = await knex.schema.hasTable("proposal_keys");
    if(!hasTable){
        return knex.schema.createTable("proposal_keys",(table)=>{
            table.increments();
            table.string("UUID");
            table.integer("creator_id").unsigned();
            table.integer("spouse_id").unsigned();
            table.foreign('creator_id').references('users.id')
            table.foreign('spouse_id').references('users.id')
            table.timestamps(false,true);
        });
    }else{
        return Promise.resolve();
    }
}


export async function down(knex: Knex){
    return knex.schema.dropTableIfExists("proposal_keys");
}

