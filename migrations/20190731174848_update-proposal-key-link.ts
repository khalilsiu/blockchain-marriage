import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    return knex.schema.alterTable('proposal_keys',(table)=>{
        table.integer('certificate_id').unsigned;
        table.foreign('certificate_id').references('certificates.id')
    })
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.alterTable('proposal_keys',(table)=>{
        table.dropColumn('certificate_id');
    })
}

