import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    return knex.schema.alterTable('stories',(table)=>{
        table.string('place_id');
    })
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.alterTable('stories',(table)=>{
        table.dropColumn('place_id');
    })
}

