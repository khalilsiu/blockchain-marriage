import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    const hasTable = await knex.schema.hasTable("stories");
    if(!hasTable){
        return knex.schema.createTable("stories",(table)=>{
            table.increments();
            table.text('description')
            table.integer("certificate_id").unsigned();
            table.foreign('certificate_id').references('certificates.id')
            table.timestamps(false,true);
        });
    }else{
        return Promise.resolve();
    }
}


export async function down(knex: Knex)  {
    return knex.schema.dropTableIfExists("stories");
}

