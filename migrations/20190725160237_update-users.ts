import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    return knex.schema.alterTable('users',(table)=>{
        table.renameColumn('name','first_name');
        table.renameColumn('surname','last_name');
        table.string('middle_name')
    })
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.alterTable('users',(table)=>{
        table.renameColumn('first_name','name');
        table.renameColumn('last_name','surname');
        table.dropColumn('middle_name');
    })
}

