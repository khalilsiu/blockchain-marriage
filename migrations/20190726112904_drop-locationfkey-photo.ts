import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    return knex.schema.alterTable('photos',(table)=>{
        table.dropColumn('locations_id');
    })
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.alterTable('photos',(table)=>{
        table.integer("locations_id").unsigned();
        table.foreign('locations_id').references('locations.id')
    })
}

