import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists('locations');
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.createTableIfNotExists('locations',(table)=>{
        table.increments();
        table.float('location_longitude')
        table.float('location_latitude')
    })
}

