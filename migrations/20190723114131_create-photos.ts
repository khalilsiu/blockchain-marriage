import * as Knex from "knex";


export async function up(knex: Knex) {
    const hasTable = await knex.schema.hasTable("photos");
    if(!hasTable){
        return knex.schema.createTable("photos",(table)=>{
            table.increments();
            table.text('description')
            table.string('photo')
            table.integer("locations_id").unsigned();
            table.foreign('locations_id').references('locations.id')
            table.timestamps(false,true);
        });
    }else{
        return Promise.resolve();
    }
}


export async function down(knex: Knex) {
    return knex.schema.dropTableIfExists("photos");
}

