import * as Knex from "knex";



export async function up(knex: Knex): Promise<any> {
    return knex.schema.alterTable('certificates',(table)=>{
        table.decimal('marriage_location_longitude',15,4).alter();
        table.decimal('marriage_location_latitude',15,4).alter();
    })
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.alterTable('certificates',(table)=>{
        table.float('marriage_location_longitude').alter();
        table.float('marriage_location_latitude').alter();
    })
}

