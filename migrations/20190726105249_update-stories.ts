import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    return knex.schema.alterTable('stories',(table)=>{
        table.decimal('longitude',15,4);
        table.decimal('latitude',15,4);
    })
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.alterTable('stories',(table)=>{
        table.dropColumn('longitude');
        table.dropColumn('latitude');
    })
}

