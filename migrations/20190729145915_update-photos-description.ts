import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    return knex.schema.alterTable('photos',(table)=>{
        table.dropColumn('description');
    })
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.alterTable('photos',(table)=>{
        table.text('description');
    })
}

