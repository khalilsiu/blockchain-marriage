import * as Knex from "knex";


export async function up(knex: Knex) {
    return knex.schema.alterTable('users',(table)=>{
        table.string('full_name')
    })
}


export async function down(knex: Knex){
    return knex.schema.alterTable('users',(table)=>{
        table.dropColumn('full_name')
    })
}
