import * as Knex from "knex";


export async function up(knex: Knex) {
    return knex.schema.alterTable('users',(table)=>{
        table.string('PDF_path')
    })
}


export async function down(knex: Knex){
    return knex.schema.alterTable('users',(table)=>{
        table.dropColumn('PDF_path')
    })
}
