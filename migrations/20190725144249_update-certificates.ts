import * as Knex from "knex";

export async function up(knex: Knex) {
  return knex.schema.table("certificates", table => {
    table.boolean("charged");
    table.string("receipt_url");
  });
}

export async function down(knex: Knex) {
  return knex.schema.table("certificates", table => {
    table.dropColumn("charged");
    table.dropColumn("receipt_url");
  });
}
