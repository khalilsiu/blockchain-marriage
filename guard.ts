import {Request,Response,NextFunction} from 'express';
export function isLoggedIn(req: Request,res: Response,next:NextFunction){

    if(req.user){
        next();
    }else{
        res.redirect('/login.html');
    }}

export function loginFlow(req:Request,res:Response,next:NextFunction){
        return (err:Error,user:any,info:{message:string})=>{
            if(err){
              res.redirect("/login.html?error="+err.message);
            }else if(info && info.message){
              res.redirect("/login.html?error="+info.message);
            }else{
              req.logIn(user,(err)=>{
                if(err){
                  res.redirect("/login.html?error="+"Failed to Login");
                }else{
                  res.redirect("/");
                }
              });
            }
        }
      }
    
